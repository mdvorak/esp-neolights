#pragma once

#include <Arduino.h>
#include <NeoPixelBus.h>

// TODO benchmark

template<typename T_COLOR_FEATURE>
class NeoFastBufferMethod : public NeoBufferMethod<T_COLOR_FEATURE>
{
  public:
    NeoFastBufferMethod(uint16_t width, uint16_t height)
      : NeoBufferMethod<T_COLOR_FEATURE>(width, height, NULL) {
    }

    // Replaces former implementation
    void CopyPixels(uint8_t* pPixelDest, const uint8_t* pPixelSrc, uint16_t count) {
      memcpy(pPixelDest, pPixelSrc, count * T_COLOR_FEATURE::PixelSize);
    }
};


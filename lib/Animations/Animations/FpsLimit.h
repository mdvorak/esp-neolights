#pragma once

#include <Arduino.h>

class FpsLimit {
  public:
    FpsLimit(unsigned long fps)
      : _last(0) {
      // Set FPS
      this->setFps(fps);
    }

    void wait() {
      unsigned long now = millis();
      unsigned long diff = now - _last;
      _last = now;

      if (diff < _fpsDelay) {
        delay(_fpsDelay - diff);
      }
    }

    unsigned long getFps() {
      return _fps;
    }

    void setFps(unsigned long fps) {
      if (fps < 0) fps = 30UL;

      _fpsDelay = 1000UL / fps;
      _fps = fps;
    }

  protected:
    unsigned long _fps;
    unsigned long _fpsDelay;
    unsigned long _last;
};

#pragma once

#include "../Animations.h"
#include "FpsLimit.h"
#include "Functions.h"
#include "../Internal/FastBuffer.h"


template<typename T_BUFFER_METHOD>
void generateRainbow16(T_BUFFER_METHOD &buffer, uint8_t brightness, double gamma) {
  uint32_t frameCount = buffer.Height();
  uint16_t brightness16 = brightness << 8 | brightness;
  bool gammaCorrection = gamma > 0.0 && 1.0 - gamma > 0.01; // Note: Negative gamma is not supported

#if defined(DEBUG_ESP_RAINBOW) && defined(DEBUG_ESP_PORT)
  char colorStr[10] = {};
#endif

  uint32_t steps = buffer.Width() * frameCount;
  for (uint32_t i = 0; i < steps; i++) {
    auto color = rgbColorFromHsv16(i * UINT16_MAX / (steps - 1), UINT16_MAX, brightness16);
    RgbColor corrected = gammaCorrection ? color.Correct(gamma) : color;

#if defined(DEBUG_ESP_RAINBOW) && defined(DEBUG_ESP_PORT)
    DEBUG_ESP_PORT.print(" -- ");
    DEBUG_ESP_PORT.print(i % frameCount);
    DEBUG_ESP_PORT.print(" / ");
    DEBUG_ESP_PORT.print(i / frameCount);
    DEBUG_ESP_PORT.print(" #");
    HtmlColor(corrected).ToNumericalString(colorStr, sizeof(colorStr) - 1);
    DEBUG_ESP_PORT.println(colorStr);
#endif

    buffer.SetPixelColor(i / frameCount, i % frameCount, corrected);
  }
}


template<typename T_COLOR_FEATURE>
class RainbowAnimation : public Animation<T_COLOR_FEATURE>
{
  private:
    using BufferMethod = NeoFastBufferMethod<T_COLOR_FEATURE>;

  public:
    RainbowAnimation(const AnimationConfig& config)
      : _buffer(constrain(config.count, 3, 2000), constrain(config.frames, 1, 10)),
        _index(0),
        _limit(constrain(config.fps, 1, 100))
    {
      // Fill buffer
      uint8_t brightness = map(constrain(config.brightness, 0, 100), 0, 100, 0, 255);
      generateRainbow16(_buffer, brightness, config.gamma);
    }

    virtual void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer) override {
      // Copy pixels
      uint16_t frames = _buffer.Height();
      bltPixelsRotate<BufferMethod>(_buffer, _index % frames, destBuffer, _index / frames);

      // Move
      ++_index;

      // Wait for next frame
      _limit.wait();
    }

    static const char* name() {
      return "Rainbow";
    }

    static const char* type() {
      return "x.animation.rainbow";
    }

  private:
    BufferMethod _buffer;
    uint16_t _index;
    FpsLimit _limit;
};

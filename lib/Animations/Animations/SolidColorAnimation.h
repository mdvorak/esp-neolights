#pragma once

#include "../Animations.h"


// This animation is intended mostly for led tests
template<typename T_COLOR_FEATURE>
class SolidColorAnimation : public Animation<T_COLOR_FEATURE>
{
  public:
    SolidColorAnimation(const AnimationConfig& config)
      : _color(config.color)
    {
    }

    virtual void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer) override {
      uint16_t destCount = destBuffer.PixelCount();
      uint8_t* pixels = destBuffer.Pixels;

      if (destCount < 1)
        return;

      // First pixel
      T_COLOR_FEATURE::applyPixelColor(pixels, 0, _color);
      // Fill buffer
      T_COLOR_FEATURE::replicatePixel(T_COLOR_FEATURE::getPixelAddress(pixels, 1), pixels, destCount - 1);

      // Wait a little
      delay(5);
    }

    static const char* name() {
      return "SolidColor";
    }

    static const char* type() {
      return "x.animation.solidcolor";
    }

  private:
    typename T_COLOR_FEATURE::ColorObject _color;
};

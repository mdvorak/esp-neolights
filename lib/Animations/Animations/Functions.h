#pragma once

#include <Arduino.h>
#include <NeoPixelBus.h>

struct Rgb16Color {

  Rgb16Color(uint16_t r, uint16_t g, uint16_t b)
    : R(r), G(g), B(b)
  {
  }

  Rgb16Color()
    : R(0), G(0), B(0)
  {
  }

  bool operator==(const Rgb16Color& other) const
  {
    return (R == other.R && G == other.G && B == other.B);
  };

  bool operator!=(const Rgb16Color& other) const
  {
    return !(*this == other);
  };

  operator RgbColor() const {
    return RgbColor(R >> 8, G >> 8, B >> 8);
  }

  RgbColor Correct(double gamma) const;

  uint16_t R;
  uint16_t G;
  uint16_t B;
};


Rgb16Color rgbColorFromHsv16(uint16_t h, uint16_t s, uint16_t v);


template<typename T_BUFFER_METHOD>
void bltPixels(T_BUFFER_METHOD &srcBuffer, uint16_t srcIndex, uint16_t srcFrame, NeoBufferContext<typename T_BUFFER_METHOD::ColorFeature> destBuffer, uint16_t destIndex) {
  // Normalize coords
  uint16_t srcCount = srcBuffer.Width();
  uint16_t destCount = destBuffer.PixelCount();

  if (srcIndex >= srcCount || destIndex >= destCount) {
    return;
  }

  uint16_t count = std::min(srcCount - srcIndex, destCount - destIndex);
  srcFrame = srcFrame % srcBuffer.Height();

  // Perform copy
  uint8_t* pSrc = T_BUFFER_METHOD::ColorFeature::getPixelAddress(srcBuffer.Pixels(), srcIndex + srcFrame * srcCount);
  uint8_t* pDest = T_BUFFER_METHOD::ColorFeature::getPixelAddress(destBuffer.Pixels, destIndex);

  srcBuffer.CopyPixels(pDest, pSrc, count);
}


template<typename T_BUFFER_METHOD>
void bltPixelsRotate(T_BUFFER_METHOD &srcBuffer, uint16_t srcFrame, NeoBufferContext<typename T_BUFFER_METHOD::ColorFeature> destBuffer, uint16_t destIndex) {
  uint16_t srcCount = srcBuffer.Width();
  uint16_t destCount = destBuffer.PixelCount();

  // Within limits
  destIndex = destIndex % srcCount;

  // First part of the destBuffer
  bltPixels<T_BUFFER_METHOD>(srcBuffer, srcCount - destIndex, srcFrame, destBuffer, 0);

  // Rest of the destBuffer
  for (uint16_t i = destIndex; i < destCount; i += srcCount) {
    bltPixels<T_BUFFER_METHOD>(srcBuffer, 0, srcFrame, destBuffer, i);
  }
}

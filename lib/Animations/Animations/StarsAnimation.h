#pragma once

#include <DebugEsp.h>
#include "../Animations.h"
#include "FpsLimit.h"
#include "Functions.h"

// TODO brightness
// TODO configure fade speed
// TODO try using NeoEase

template<typename T_COLOR_FEATURE>
class StarsAnimation : public Animation<T_COLOR_FEATURE>
{
  public:
    StarsAnimation(const AnimationConfig& config)
      : _limit(constrain(config.fps, 1, 100)),
        _colors(config.colors),
       _count(config.count),
       _brightness(constrain(config.brightness, 1, 100) / 100.0)
    {
    }

    virtual void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer) override {
      uint16_t destCount = destBuffer.PixelCount();
      uint8_t* pixels = destBuffer.Pixels;

      int stars = 0;

      for (uint16_t i = 0; i < destCount; i++) {
        auto color = T_COLOR_FEATURE::retrievePixelColor(pixels, i);

        // Empty pixel
        // TODO relies on RgbColor
        if (!color.R && !color.G && !color.B) {
          continue;
        }

        // We found star
        stars++;

        // Set new value
        animateStar(destBuffer, i, color);
      }

      // Add new stars
      // TODO random does not work well for higher counts
      if (stars < _count && (rand() % _count) > stars) {
        ColorObject color = _colors ? HsbColor((rand() % 360) / 360.0, 1, _brightness) : HsbColor(0, 0, _brightness);
        T_COLOR_FEATURE::applyPixelColor(pixels, rand() % destCount, color);
      }

      // Wait for next frame
      _limit.wait();
    }

    static const char* name() {
      return "Stars";
    }

    static const char* type() {
      return "x.animation.stars";
    }

  private:
    using ColorObject = typename T_COLOR_FEATURE::ColorObject;

  protected:
    FpsLimit _limit;
    bool _colors;
    int8_t _count;
    float _brightness;

    virtual void animateStar(NeoBufferContext<T_COLOR_FEATURE> destBuffer, uint16_t index, ColorObject& color)
    {
      // Recolor
      color.Darken(2);
      T_COLOR_FEATURE::applyPixelColor(destBuffer.Pixels, index, color);
    }
};


template<typename T_COLOR_FEATURE>
class ShootingStarsAnimation : public StarsAnimation<T_COLOR_FEATURE>
{
  public:
      ShootingStarsAnimation(const AnimationConfig& config)
        : StarsAnimation<T_COLOR_FEATURE>(config)
      {
      }

      static const char* name() {
        return "Shooting Stars";
      }

    private:
      using ColorObject = typename T_COLOR_FEATURE::ColorObject;

    protected:
      virtual void animateStar(NeoBufferContext<T_COLOR_FEATURE> destBuffer, uint16_t index, ColorObject& color) override
      {
        // Show new
        color.Darken(10);
        if (index > 0) {
          T_COLOR_FEATURE::applyPixelColor(destBuffer.Pixels, index - 1, color);
        }

        color.Darken(10);
        T_COLOR_FEATURE::applyPixelColor(destBuffer.Pixels, index, color);
      }
};

#pragma once

#include <Arduino.h>
#include <NeoPixelBus.h>
#include "AnimationConfig.h"


// Animation class
template<typename T_COLOR_FEATURE>
class Animation
{
  public:
    virtual void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer) = 0;
    virtual ~Animation() {
    }
};


/// NoAnimation class
template<typename T_COLOR_FEATURE>
class NoAnimation : public Animation<T_COLOR_FEATURE>
{
  public:
    NoAnimation(const AnimationConfig& config) {
    }

    virtual void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer) override {
      delay(1);
    }

    static const char* name() {
      return NULL;
    }

    static const char* type() {
      return "x.animation.none";
    }
};


/// AnimationFactory class
template<typename T_COLOR_FEATURE>
class AnimationFactory
{
  public:
    virtual Animation<T_COLOR_FEATURE>* create(const AnimationConfig& config) = 0;

    virtual const char* name() const = 0;

    virtual const char* type() const = 0;
};


/// AnimationFactoryImpl class
template<typename T_COLOR_FEATURE, template<typename> class T_ANIMATION_TYPE>
class AnimationFactoryImpl : public AnimationFactory<T_COLOR_FEATURE>
{
  public:
    virtual Animation<T_COLOR_FEATURE>* create(const AnimationConfig& config) override
    {
      return new T_ANIMATION_TYPE<T_COLOR_FEATURE>(config);
    }

    virtual const char* name() const override
    {
      return T_ANIMATION_TYPE<T_COLOR_FEATURE>::name();
    }

    virtual const char* type() const override
    {
      return T_ANIMATION_TYPE<T_COLOR_FEATURE>::type();
    }
};

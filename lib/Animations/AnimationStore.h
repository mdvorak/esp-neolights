#pragma once

#include "AnimationConfig.h"

// AnimationInfo class
struct AnimationInfo
{
  const char* type;
  const char* name;
};


// AnimationStore class
class AnimationStore
{
  public:
    static const uint8_t DISABLED = 0xFF;

    virtual uint8_t animationSlotsCount() const = 0;

    virtual uint8_t animationsCount() const = 0;

    virtual void animationInfo(uint8_t animationId, AnimationInfo& out) const = 0;

    virtual void loadAnimationConfig(uint8_t slot, AnimationConfig& animationConfig) const = 0;

    virtual void storeAnimationConfig(uint8_t slot, const AnimationConfig& animationConfig) = 0;

    virtual uint8_t selectedSlot() const = 0;

    virtual void selectSlot(uint8_t slot, bool persist = true) = 0;

    virtual uint8_t savedSlot() const = 0;

    virtual void releaseMemory() {
    }

    virtual void factoryReset() = 0;

    virtual unsigned long lastChange() const = 0;
};

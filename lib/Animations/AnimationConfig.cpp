#include "AnimationConfig.h"
#include <EEPROM.h>
#include <DebugEsp.h>


/// AnimationConfig class
void AnimationConfig::clear()
{
  memset(label, 0, sizeof(label));
  id = 0;
  brightness = 0;
  gamma = 0;
  count = 0;
  fps = 0;
  frames = 0;
  color = RgbColor();
  colors = false;
  reverse = 0;
}

static void getRgbColor(size_t& eepromOffset, RgbColor& color)
{
  EEPROM.get(eepromOffset++, color.R);
  EEPROM.get(eepromOffset++, color.G);
  EEPROM.get(eepromOffset++, color.B);
}

static void putRgbColor(size_t& eepromOffset, const RgbColor& color)
{
  EEPROM.put(eepromOffset++, color.R);
  EEPROM.put(eepromOffset++, color.G);
  EEPROM.put(eepromOffset++, color.B);
}

void AnimationConfig::load(size_t eepromOffset)
{
  const size_t initialOffset = eepromOffset;

  // Id
  EEPROM.get(eepromOffset, id); eepromOffset += sizeof(id);

  // Label - write whats needed, but move offset always by same value
  for (size_t i = 0; i < sizeof(label) - 1; i++) {
    label[i] = EEPROM.read(eepromOffset++);
  }
  label[sizeof(label) - 1] = '\0';

  // Don't return invalid name
  if (label[0] == 255) {
    label[0] = '\0';
  }

  // All other properties
  EEPROM.get(eepromOffset, brightness); eepromOffset += sizeof(brightness);
  EEPROM.get(eepromOffset, gamma); eepromOffset += sizeof(gamma);
  EEPROM.get(eepromOffset, count); eepromOffset += sizeof(count);
  EEPROM.get(eepromOffset, fps); eepromOffset += sizeof(fps);
  EEPROM.get(eepromOffset, frames); eepromOffset += sizeof(frames);

  // Color in RGB format
  getRgbColor(eepromOffset, color);

  EEPROM.get(eepromOffset, colors); eepromOffset += sizeof(colors);
  EEPROM.get(eepromOffset, reverse); eepromOffset += sizeof(reverse);

  // MUST BE LAST
  DEBUG_ESP_ASSERT(eepromOffset - initialOffset <= EEPROM_SIZE);
}

void AnimationConfig::persist(size_t eepromOffset) const
{
  const size_t initialOffset = eepromOffset;

  // Id
  EEPROM.put(eepromOffset, id); eepromOffset += sizeof(id);

  // Label - write whats needed, but move offset always by same value
  for (size_t i = 0; i < sizeof(label) - 1; i++) {
    EEPROM.put(eepromOffset + i, label[i]);

    if (!label[i])
      break;
  }
  eepromOffset += sizeof(label) - 1;

  // All other properties
  EEPROM.put(eepromOffset, brightness); eepromOffset += sizeof(brightness);
  EEPROM.put(eepromOffset, gamma); eepromOffset += sizeof(gamma);
  EEPROM.put(eepromOffset, count); eepromOffset += sizeof(count);
  EEPROM.put(eepromOffset, fps); eepromOffset += sizeof(fps);
  EEPROM.put(eepromOffset, frames); eepromOffset += sizeof(frames);

  // Color in RGB format
  putRgbColor(eepromOffset, color);

  EEPROM.put(eepromOffset, colors); eepromOffset += sizeof(colors);
  EEPROM.put(eepromOffset, reverse); eepromOffset += sizeof(reverse);

  // MUST BE LAST
  DEBUG_ESP_ASSERT(eepromOffset - initialOffset <= EEPROM_SIZE);
}

void AnimationConfig::printTo(Print& out) const
{
  // This prevents buffer overflow in case of invalid initialization of the struct
  char safeLabel[LABEL_LENGTH + 1];
  strncpy(safeLabel, label, LABEL_LENGTH);
  safeLabel[sizeof(safeLabel) - 1] = '\0';

  // Convert to user-friendly text format
  char colorStr[10] = {};
  HtmlColor(color).ToNumericalString(colorStr, sizeof(colorStr) - 1);

  // Dump all properties
  out.print(F("id="));
  out.print(id);
  out.print(F(", label="));
  out.print(safeLabel);
  out.print(F(", brightness="));
  out.print(brightness);
  out.print(F(", gamma="));
  out.print(gamma);
  out.print(F(", count="));
  out.print(count);
  out.print(F(", fps="));
  out.print(fps);
  out.print(F(", frames="));
  out.print(frames);
  out.print(F(", color="));
  out.print(colorStr);
  out.print(F(", colors="));
  out.print(colors);
  out.print(F(", reverse="));
  out.print(reverse);
}

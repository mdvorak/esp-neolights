#pragma once

#include <Arduino.h>
#include <vector>
#include <EEPROM.h>
#include <DebugEsp.h>
#include "Animations.h"
#include "AnimationStore.h"


/// AnimationManager class
template<typename T_COLOR_FEATURE>
class AnimationManager : public AnimationStore
{
  public:
    static const size_t ANIMATION_CONFIG_OFFSET = 16;
    static const uint8_t MAX_SLOTS = 64;

    AnimationManager(size_t eepromOffset, uint8_t animationSlotsCount)
      : _eepromOffset(eepromOffset),
        _animationSlotsCount(constrain(animationSlotsCount, 1, MAX_SLOTS))
    {
      // Always have NoAnimation at zero index
      registerAnimation<NoAnimation>();

      // Get active slot
      EEPROM.get(_eepromOffset, _activeSlot);
    }

    size_t eepromSize() const
    {
      return ANIMATION_CONFIG_OFFSET + _animationSlotsCount * AnimationConfig::EEPROM_SIZE;
    }

    virtual uint8_t animationSlotsCount() const override
    {
      return _animationSlotsCount;
    }

    virtual uint8_t animationsCount() const override
    {
      return _factories.size();
    }

    template<template<typename> class T_ANIMATION_TYPE>
    void registerAnimation()
    {
      AnimationFactory<T_COLOR_FEATURE>* factory = new AnimationFactoryImpl<T_COLOR_FEATURE, T_ANIMATION_TYPE>();
      registerAnimation(factory);
    }

    void registerAnimation(AnimationFactory<T_COLOR_FEATURE>* factory)
    {
      DEBUG_ESP_ASSERT(factory != NULL);
      _factories.push_back(factory);

      _lastChange = millis();
      DEBUG_ESP_PRINTF("ADDED ANIMATION %s AS %d\n", factory->name() != NULL ? factory->name() : "NoAnimation", _factories.size() - 1);
    }

    AnimationFactory<T_COLOR_FEATURE>* animationFactory(uint8_t animationId) const
    {
      return animationId < _factories.size() ? _factories[animationId] : NULL;
    }

    virtual void animationInfo(uint8_t animationId, AnimationInfo& out) const override
    {
      auto* f = animationFactory(animationId);

      if (f) {
        out.name  = f->name();
        out.type = f->type();
      } else {
        out.name  = NULL;
        out.type = NULL;
      }
    }

    void animate(NeoBufferContext<T_COLOR_FEATURE> destBuffer)
    {
      if (!_animation) {
        initAnimation();
      }

      _animation->animate(destBuffer);
    }

    virtual void loadAnimationConfig(uint8_t slot, AnimationConfig& animationConfig) const override
    {
      DEBUG_ESP_PRINTF("*REQUESTED CONFIG FOR SLOT %d\n", slot);

      // Special case - force empty animation
      if (slot == DISABLED) {
        DEBUG_ESP_PRINTLN("*EMPTY ANIMATION");
        animationConfig.clear();
        return;
      }

      slot = slot < _animationSlotsCount ? slot : _animationSlotsCount;
      animationConfig.load(animationConfigAddress(slot));

#ifdef DEBUG_ESP_PORT
      // Print to serial as json
      DEBUG_ESP_PORT.print("*LOADING ANIMATION FROM SLOT ");
      DEBUG_ESP_PORT.println(slot);
      DEBUG_ESP_PORT.print(" ");
      animationConfig.printTo(DEBUG_ESP_PORT);
      DEBUG_ESP_PORT.println();
#endif
    }

    virtual void storeAnimationConfig(uint8_t slot, const AnimationConfig& animationConfig) override
    {
      slot = slot < _animationSlotsCount ? slot : _animationSlotsCount;

#ifdef DEBUG_ESP_PORT
      // Print to serial as json
      DEBUG_ESP_PORT.print("*STORING ANIMATION TO SLOT ");
      DEBUG_ESP_PORT.println(slot);

      animationConfig.printTo(DEBUG_ESP_PORT);
      DEBUG_ESP_PORT.println();
#endif

      // Persist animation config
      animationConfig.persist(animationConfigAddress(slot));
      EEPROM.commit();
      _lastChange = millis();
    }

    void resetActiveAnimation() {
      DEBUG_ESP_PRINTLN("RESSETING ACTIVE ANIMATION");

      // Update
      selectSlot(DISABLED);
    }

    virtual uint8_t selectedSlot() const override
    {
      return _activeSlot;
    }

    virtual uint8_t savedSlot() const override
    {
      uint8_t activeSlot = DISABLED;
      EEPROM.get(_eepromOffset, activeSlot);
      return activeSlot;
    }

    virtual void selectSlot(uint8_t slot, bool persist = true) override
    {
      // Only if changes
      if (_activeSlot != slot) {
        DEBUG_ESP_PRINT("*SETTING ACTIVE SLOT TO ");
        DEBUG_ESP_PRINTLN(slot);

        // Persist selected animation
        if (persist) {
          EEPROM.put(_eepromOffset, slot);
          EEPROM.commit();
          DEBUG_ESP_PRINTLN("*ACTIVE SLOT SAVED");
        }

        // Update variable
        _activeSlot = slot;
        _lastChange = millis();

        // Forces animation re-creation
        releaseMemory();
      }
    }

    virtual void releaseMemory() override
    {
      if (_animation) {
        delete _animation;
        _animation = NULL;
      }

      DEBUG_ESP_PRINTLN("*RELEASED MEMORY");
    }

    virtual void factoryReset()
    {
      // Clear EEPROM
      size_t size = eepromSize();

      for (size_t i = 0; i < size; i++) {
        if (EEPROM.read(_eepromOffset + i)) {
          EEPROM.write(_eepromOffset + i, 0);
        }
      }

      EEPROM.commit();
      _lastChange = millis();
    }

    virtual unsigned long lastChange() const override
    {
      return _lastChange;
    }

  private:
    size_t _eepromOffset;
    uint8_t _animationSlotsCount;
    std::vector<AnimationFactory<T_COLOR_FEATURE>*> _factories;
    uint8_t _activeSlot = DISABLED;
    Animation<T_COLOR_FEATURE>* _animation = NULL;
    unsigned long _lastChange = 0;

    inline size_t animationConfigAddress(uint8_t slot) const
    {
      return _eepromOffset + ANIMATION_CONFIG_OFFSET + slot * AnimationConfig::EEPROM_SIZE;
    }

    void initAnimation()
    {
      // Make sure previous is released
      if (_animation) {
        delete _animation;
        _animation = NULL;
      }

      DEBUG_ESP_PRINTLN("INITIALIZING ANIMATION");

      // Load config
      AnimationConfig config = {};
      loadAnimationConfig(_activeSlot, config);
      const char* name = NULL;

      // Create new animation
      auto* f = animationFactory(config.id);
      if (f) {
        // Create animation
        _animation = f->create(config);
        name = f->name();
      } else {
        _animation = new NoAnimation<T_COLOR_FEATURE>(config);
        name = NoAnimation<T_COLOR_FEATURE>::name();
      }

      DEBUG_ESP_PRINT("CREATED ANIMATION ");
      DEBUG_ESP_PRINT(name ? name : "NoAnimation");
      DEBUG_ESP_PRINT(" IN SLOT ");
      DEBUG_ESP_PRINTLN(_activeSlot);
      DEBUG_ESP_PRINTLN();
    }
};

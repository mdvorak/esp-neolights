#include "Animations/Functions.h"

Rgb16Color rgbColorFromHsv16(uint16_t h, uint16_t s, uint16_t v)
{
  uint16_t region;
  uint64_t fpart; // Note: This is crucial, we need it to be 64bit otherwise it will be truncated
  uint16_t p, q, t;

  if (s == 0) {
    /* color is grayscale */
    return Rgb16Color(v, v, v);
  }

  /* make hue 0-5 */
  region = h / 10923U;
  /* find remainder part, make it from 0-65535 */
  fpart = (h - (region * 10923U)) * 6U;

  /* calculate temp vars, doing integer multiplication */
  p = (v * (65535U - s)) >> 16;
  q = (v * (65535U - (uint16_t)((s * fpart) >> 16))) >> 16;
  t = (v * (65535U - (uint16_t)((s * ((65535U - fpart))) >> 16))) >> 16;

  /* assign temp vars based on color cone region */
  switch (region) {
    case 0:
      return Rgb16Color(v, t, p);
    case 1:
      return Rgb16Color(q, v, p);
    case 2:
      return Rgb16Color(p, v, t);
    case 3:
      return Rgb16Color(p, q, v);
    case 4:
      return Rgb16Color(t, p, v);
    default:
      return Rgb16Color(v, p, q);
  }
}


static inline uint8_t gammaCorrect(uint16_t value, double gamma) {
  double y = (double)(value) / 65535.0;
  y = pow(y, 1.0 / gamma);
  return static_cast<uint8_t>(floor(255.0 * y + 0.5));
}

// TODO move in external configurable class
RgbColor Rgb16Color::Correct(double gamma) const
{
  return RgbColor(gammaCorrect(R, gamma), gammaCorrect(G, gamma), gammaCorrect(B, gamma));
}

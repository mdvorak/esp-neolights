#pragma once

#include <Arduino.h>
#include <NeoPixelBus.h>

struct AnimationConfig
{
  static const size_t EEPROM_SIZE = 64;
  static const size_t LABEL_LENGTH = 20;

  uint8_t id;
  char label[LABEL_LENGTH + 1];

  uint8_t brightness;
  double gamma;
  uint16_t count;
  uint8_t fps;
  uint8_t frames;
  RgbColor color;
  bool colors;
  bool reverse;

  void clear();
  void load(size_t eepromOffset);
  void persist(size_t eepromOffset) const;

  void printTo(Print& out) const;
};

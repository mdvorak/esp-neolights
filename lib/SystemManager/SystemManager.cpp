#include <Arduino.h>
#include <Esp.h>
#include <EEPROM.h>
#include <NTPClient.h>
#include <DebugEsp.h>
#include "SystemManager.h"


/// SystemConfig class
void SystemConfig::load(size_t eepromOffset) {
  // Pixels
  EEPROM.get(eepromOffset, pixels); eepromOffset += sizeof(pixels);
  pixels = constrain(pixels, 1, 1000);

  // TimeOffset
  EEPROM.get(eepromOffset, timeOffset); eepromOffset += sizeof(timeOffset);
  timeOffset = constrain(timeOffset, -720, 720); // in minutes

  // NTP update interval
  EEPROM.get(eepromOffset, ntpUpdateInterval); eepromOffset += sizeof(ntpUpdateInterval);
  ntpUpdateInterval = constrain(ntpUpdateInterval, 1, 10080);  // in minutes

  // Reserved
  eepromOffset += 7;

  // Scheduler
  EEPROM.get(eepromOffset, schedulerEnabled), eepromOffset += sizeof(schedulerEnabled);

  // Schedule
  for (uint8_t i = 0; i < SCHEDULE_LENGTH; i++) {
    EEPROM.get(eepromOffset++, schedule[i]);
  }
}

void SystemConfig::persist(size_t eepromOffset) const {
  EEPROM.put(eepromOffset, pixels); eepromOffset += sizeof(pixels);
  EEPROM.put(eepromOffset, timeOffset); eepromOffset += sizeof(timeOffset);
  EEPROM.put(eepromOffset, ntpUpdateInterval); eepromOffset += sizeof(ntpUpdateInterval);

  // Reserved
  eepromOffset += 7;

  // Scheduler
  EEPROM.put(eepromOffset, schedulerEnabled), eepromOffset += sizeof(schedulerEnabled);

  // Schedule
  for (uint8_t i = 0; i < SCHEDULE_LENGTH; i++) {
    EEPROM.put(eepromOffset++, schedule[i]);
  }
}

bool SystemConfig::operator==(const SystemConfig& other)
{
  return this->pixels == other.pixels &&
    this->timeOffset == other.timeOffset &&
    this->ntpUpdateInterval == other.ntpUpdateInterval &&
    this->schedulerEnabled == other.schedulerEnabled &&
    this->schedule == other.schedule;
}


/// SystemManager class
SystemManager::SystemManager(size_t eepromOffset, int powerRelayPin)
  : _eepromOffset(eepromOffset),
    _powerRelayPin(powerRelayPin)
{
}

void SystemManager::begin(NTPClient& timeClient)
{
  _timeClient = &timeClient;

  // Load config
  SystemConfig config = {};
  loadConfig(config);

  // Initialize time
  DEBUG_ESP_PRINT(F("NTP OFFSET "));
  DEBUG_ESP_PRINT(config.timeOffset);
  DEBUG_ESP_PRINTLN(F(" MIN"));

  _timeOffset = config.timeOffset * 60; // from minutes to seconds
  _timeClient->setTimeOffset(_timeOffset);
  _timeClient->setUpdateInterval(config.ntpUpdateInterval * 60000); // from minutes to milliseconds

  // Remember scheduler config
  _schedulerEnabled = config.schedulerEnabled;
  _schedule = config.schedule;

  // Initalized
  DEBUG_ESP_PRINTLN(F("SYSTEM MANAGER INITIALIZED"));
}

void SystemManager::loadConfig(SystemConfig& config) const
{
  config.load(_eepromOffset);
}

void SystemManager::persistConfig(const SystemConfig& config)
{
  DEBUG_ESP_PRINTLN(F("*SAVING SYSTEM CONFIG"));

  config.persist(_eepromOffset);
  EEPROM.commit();

  _schedulerEnabled = config.schedulerEnabled;
  _schedule = config.schedule;
}

void SystemManager::enable()
{
  if (digitalRead(_powerRelayPin))
    return;

  DEBUG_ESP_PRINTLN(F("*TURNING ON LED POWER SOURCE"));
  digitalWrite(_powerRelayPin, HIGH);

  // Safety delay to protect relay
  delay(50);
}

void SystemManager::disable()
{
  if (!digitalRead(_powerRelayPin))
    return;

  DEBUG_ESP_PRINTLN(F("*TURNING OFF LED POWER SOURCE"));
  digitalWrite(_powerRelayPin, LOW);

  // Safety delay to protect relay
  delay(50);
}

void SystemManager::restart()
{
  DEBUG_ESP_PRINTLN(F("*RESTARTING ESP"));

  disable();
  EEPROM.end();

  delay(10);
  ESP.restart();
}

unsigned long SystemManager::getEpochTime() const
{
  DEBUG_ESP_ASSERT(_timeClient != NULL);

  // Move it back to UTC
  return _timeClient->getEpochTime() - _timeOffset;
}

uint8_t SystemManager::getCurrentSchedule() const
{
  // Get current time (in seconds) in local time-zone
  DEBUG_ESP_ASSERT(_timeClient != NULL);
  auto epochTime = _timeClient->getEpochTime();

  // Number of half-hours of the day (0-47)
  auto slot = (epochTime  % 86400L) / 1800;

  DEBUG_ESP_ASSERT(slot >= 0);
  DEBUG_ESP_ASSERT(slot < SystemConfig::SCHEDULE_LENGTH);

  // Return slot
  return slot;
}

uint8_t SystemManager::getScheduledSlot() const
{
  // Get current schedule
  auto slot = getCurrentSchedule();

  // Corresponding slot
  return _schedule[slot];
}

void SystemManager::suppressScheduler()
{
  DEBUG_ESP_PRINTLN(F("*SUPPRESSING SCHEDULER"));
  _schedulerEnabled = false;
  // Don't persist
}

void SystemManager::restoreScheduler()
{
  // Load config
  SystemConfig config = {};
  loadConfig(config);

  // Restore scheduler state
  _schedulerEnabled = config.schedulerEnabled;

  DEBUG_ESP_PRINT(F("*SCHEDULER RESTORED TO STATE "));
  DEBUG_ESP_PRINTLN(_schedulerEnabled ? F("ENABLED") : F("DISABLED"));
}

void SystemManager::factoryReset()
{
  // Clear EEPROM
  for (size_t i = 0; i < SystemConfig::EEPROM_SIZE; i++) {
    if (EEPROM.read(_eepromOffset + i)) {
      EEPROM.write(_eepromOffset + i, 0);
    }
  }

  EEPROM.commit();
}

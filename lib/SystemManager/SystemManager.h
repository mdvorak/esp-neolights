#pragma once

#include <array>

// From NTPClient.h
class NTPClient;


struct SystemConfig
{
  static const size_t EEPROM_SIZE = 64;
  static const size_t SCHEDULE_LENGTH = 48;

  using ScheduleArray = std::array<uint8_t, SCHEDULE_LENGTH>;

  uint16_t pixels;
  int16_t timeOffset; // in minutes
  uint32_t ntpUpdateInterval; // in minutes
  bool schedulerEnabled;
  ScheduleArray schedule; // Selected slot for each hour of the day

  void load(size_t eepromOffset);
  void persist(size_t eepromOffset) const;

  bool operator==(const SystemConfig& other);
  inline bool operator!=(const SystemConfig& other)
  {
    return !(*this == other);
  }
};


class SystemManager
{
  public:
    SystemManager(size_t eepromOffset, int powerRelayPin);

    void begin(NTPClient& timeClient);

    void loadConfig(SystemConfig& config) const;
    void persistConfig(const SystemConfig& config);

    void enable();
    void disable();

    void restart();

    // Returns epoch time in seconds, in UTC
    unsigned long getEpochTime() const;

    inline bool schedulerEnabled() const
    {
      return _schedulerEnabled;
    }

    uint8_t getCurrentSchedule() const;

    uint8_t getScheduledSlot() const;

    void suppressScheduler();

    void restoreScheduler();

    void factoryReset();

  private:
    size_t _eepromOffset;
    int _powerRelayPin;
    NTPClient* _timeClient = NULL;
    int _timeOffset = 0;

    bool _schedulerEnabled = false;
    SystemConfig::ScheduleArray _schedule = {};
};

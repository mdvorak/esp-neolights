#pragma once

#ifdef DEBUG_ESP_PORT
#define DEBUG_ESP_PRINT(...) DEBUG_ESP_PORT.print( __VA_ARGS__ )
#define DEBUG_ESP_PRINTLN(...) DEBUG_ESP_PORT.println( __VA_ARGS__ )
#define DEBUG_ESP_PRINTF(...) DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#define DEBUG_ESP_ASSERT(e) ;if(!(e)) {DEBUG_ESP_PORT.printf("\nassertion failed at " __FILE__ ":%d: " #e "\n", __LINE__);abort();}
#else
#define DEBUG_ESP_PRINT(...)
#define DEBUG_ESP_PRINTLN(...)
#define DEBUG_ESP_PRINTF(...)
#define DEBUG_ESP_ASSERT(e)
#endif

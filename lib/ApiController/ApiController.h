#pragma once

#include <ESP8266WebServer.h>
#include <ArduinoJson.h>


class ApiController
{
  public:
    ApiController(ESP8266WebServer& server);

    void requireAuthentication(const char* username, const char* password);

    inline ESP8266WebServer& server() const
    {
      return _server;
    }

  protected:
    void sendStatus(int status);
    void sendRedirect(int status, const String& url);
    void sendJson(JsonObject& root, const String& type);

    bool authenticate();

    bool handleOptions(const String& methods);
    void sendAccessControlHeaders(const String& methods);
    void sendETag(const String& value);
    String parseIfNoneMatch() const;
    bool ifNoneMatchEquals(const String& value);

  public:
    // Constants
    static const char HEADER_AUTHORIZATION[];
    static const char HEADER_LOCATION[];
    static const char HEADER_CACHE_CONTROL[];
    static const char HEADER_ALLOW_P[] PROGMEM;
    static const char HEADER_ETAG_P[] PROGMEM;
    static const char HEADER_IF_NONE_MATCH[];
    static const char HEADER_X_FORWARDED_PROTO[];

    static const char HEADER_ACCESS_CONTROL_ALLOW_ORIGIN_P[] PROGMEM;
    static const char HEADER_ACCESS_CONTROL_ALLOW_METHODS_P[] PROGMEM;
    static const char HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS_P[] PROGMEM;
    static const char HEADER_ACCESS_CONTROL_ALLOW_HEADERS_P[] PROGMEM;
    static const char HEADER_ACCESS_CONTROL_MAX_AGE_P[] PROGMEM;

    static const char ETAG_PREFIX[];

    static const char KEY_ITEMS[];
    static const char KEY_LINKS[];
    static const char LINK_SELF[];

  private:
    ESP8266WebServer& _server;
    String _authorization;

    static String encodeBasicAuth(const char* username, const char* password);
};

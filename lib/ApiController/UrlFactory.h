#pragma once

#include <ESP8266WebServer.h>
#include <DebugEsp.h>

class UrlFactory
{
public:
  static const char HEADER_X_FORWARDED_PROTO[];

  UrlFactory(ESP8266WebServer &server)
      : _prefix(currentPrefix(server))
  {
  }

  String absoluteUrl(const char *relativeUrl)
  {
    return _prefix + relativeUrl;
  }

  String absoluteUrl(const String &relativeUrl)
  {
    return _prefix + relativeUrl;
  }

  static String currentPrefix(ESP8266WebServer &server)
  {
    // Hostname
    String host = server.hostHeader();
    if (host.length() < 1)
    {
      host = server.client().localIP().toString();
      DEBUG_ESP_PRINT("UrlFactory found Host ");
      DEBUG_ESP_PRINTLN(host);
    }

    // Protocol
    String proto;
    if (server.hasHeader(HEADER_X_FORWARDED_PROTO))
    {
      DEBUG_ESP_PRINT("UrlFactory found ");
      DEBUG_ESP_PRINT(HEADER_X_FORWARDED_PROTO);
      DEBUG_ESP_PRINT(" ");

      proto = server.header(HEADER_X_FORWARDED_PROTO) + "://";
      DEBUG_ESP_PRINTLN(proto);
    }
    else
    {
      DEBUG_ESP_PRINTLN("UrlFactory using default http://");
      proto = "http://";
    }

    // Join
    return proto + host;
  }

private:
  String _prefix;
};

#include "ApiController.h"
#include "WiFiClientPrint.h"
#include <libb64/cencode.h>
#include <DebugEsp.h>

#define JSON_BUFFER_SIZE 64

// Constants
const char ApiController::HEADER_AUTHORIZATION[] = "Authorization";
const char ApiController::HEADER_LOCATION[] = "Location";
const char ApiController::HEADER_CACHE_CONTROL[] = "Cache-Control";
const char ApiController::HEADER_ALLOW_P[] PROGMEM = "Allow";
const char ApiController::HEADER_ETAG_P[] PROGMEM = "ETag";
const char ApiController::HEADER_IF_NONE_MATCH[] = "If-None-Match";
const char ApiController::HEADER_X_FORWARDED_PROTO[] = "X-Forwarded-Proto";

const char ApiController::HEADER_ACCESS_CONTROL_ALLOW_ORIGIN_P[] PROGMEM = "Access-Control-Allow-Origin";
const char ApiController::HEADER_ACCESS_CONTROL_ALLOW_METHODS_P[] PROGMEM = "Access-Control-Allow-Methods";
const char ApiController::HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS_P[] PROGMEM = "Access-Control-Allow-Credentials";
const char ApiController::HEADER_ACCESS_CONTROL_ALLOW_HEADERS_P[] PROGMEM = "Access-Control-Allow-Headers";
const char ApiController::HEADER_ACCESS_CONTROL_MAX_AGE_P[] PROGMEM = "Access-Control-Max-Age";

const char ApiController::ETAG_PREFIX[] = "W/\"";

const char ApiController::KEY_ITEMS[] = "items";
const char ApiController::KEY_LINKS[] = "_links";
const char ApiController::LINK_SELF[] = "self";

// Methods
ApiController::ApiController(ESP8266WebServer& server)
  : _server(server)
{
}

void ApiController::requireAuthentication(const char* username, const char* password)
{
  DEBUG_ESP_ASSERT(username != NULL);
  DEBUG_ESP_ASSERT(password != NULL);

  DEBUG_ESP_PRINT(F("API USER: "));
  DEBUG_ESP_PRINTLN(username);

  // Encode
  _authorization = encodeBasicAuth(username, password);

  DEBUG_ESP_PRINT(F("AUTHORIZATION: "));
  DEBUG_ESP_PRINTLN(_authorization);
}

String ApiController::encodeBasicAuth(const char* username, const char* password)
{
  DEBUG_ESP_ASSERT(username != NULL);
  DEBUG_ESP_ASSERT(password != NULL);

  String auth = String(username) + ":" + String(password);

  // Prepare buffer for base64
  size_t bufferLen = base64_encode_expected_len(auth.length()) + 4; // Note: for some reason this macro provides too small buffer
  char* buffer = new char[bufferLen];
  memset(buffer, 0, bufferLen);

  // Encode
  size_t encodeResult = base64_encode_chars(auth.c_str(), auth.length(), buffer);
  DEBUG_ESP_ASSERT(encodeResult > 0);
  DEBUG_ESP_ASSERT(encodeResult < bufferLen - 1 /* mind terminating \0 */);

  // As string
  String authorization = String(buffer);
  delete[] buffer;

  return authorization;
}

void ApiController::sendStatus(int status)
{
  DEBUG_ESP_PRINT(F("SENDING STATUS "));
  DEBUG_ESP_PRINT(status);
  DEBUG_ESP_PRINT(F(" FOR "));
  DEBUG_ESP_PRINTLN(server().uri());

  server().send(status);
}

void ApiController::sendRedirect(int status, const String& url)
{
  DEBUG_ESP_PRINT(F("SENDING "));
  DEBUG_ESP_PRINT(status);
  DEBUG_ESP_PRINT(F(" REDIRECT TO "));
  DEBUG_ESP_PRINTLN(url);

  server().sendHeader(String(HEADER_LOCATION), url, true);
  server().send(status, "", "");
  server().client().stop();
}

void ApiController::sendJson(JsonObject& root, const String& type)
{
#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.println(F("SENDING PAYLOAD:"));
  DEBUG_ESP_PORT.print(" ");
  root.printTo(DEBUG_ESP_PORT);
  DEBUG_ESP_PORT.println();
#endif

  server().setContentLength(root.measureLength());
  server().send(200, type, "");

  WiFiClientPrint<JSON_BUFFER_SIZE> p(server().client());
  root.printTo(p);
  p.stop(); // Calls flush and WifiClient.stop()

  DEBUG_ESP_PRINTLN(F("PAYLOAD WRITTEN"));
}

bool ApiController::authenticate() {
  if (!server().hasHeader(HEADER_AUTHORIZATION)) {
    return false;
  }

  String authReq = server().header(HEADER_AUTHORIZATION);
  if (authReq.startsWith("Basic")) {
    authReq = authReq.substring(6);
    authReq.trim();

    // Compare
    return authReq.equals(_authorization);
  }

  return false;
}

bool ApiController::handleOptions(const String& methods)
{
  if (server().method() != HTTP_OPTIONS) {
    return false;
  }

  // OPTIONS
  server().sendHeader(HEADER_ALLOW_P, methods);

#ifdef DEBUG_ESP_PORT
  // For UI dev server
  DEBUG_ESP_PRINT(F("SENDING OPTIONS WITH CORS: "));
  DEBUG_ESP_PRINTLN(methods);
  sendAccessControlHeaders(methods);
#endif

  server().send(204);
  return true;
}

void ApiController::sendAccessControlHeaders(const String& methods)
{
  // TODO SDK version 2.3.0 inserts this even it shouldn't, re-add when released next version
  //server().sendHeader(HEADER_ACCESS_CONTROL_ALLOW_ORIGIN_P, "*");
  server().sendHeader(HEADER_ACCESS_CONTROL_ALLOW_METHODS_P, methods);
  server().sendHeader(HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS_P, F("true"));
  server().sendHeader(HEADER_ACCESS_CONTROL_ALLOW_HEADERS_P, F("Accept,Authorization,Content-Type,Location,Origin"));
  server().sendHeader(HEADER_ACCESS_CONTROL_MAX_AGE_P, F("600"));
}

void ApiController::sendETag(const String& value)
{
  server().sendHeader(HEADER_ETAG_P, String(ETAG_PREFIX) + value + '"');
}

String ApiController::parseIfNoneMatch() const
{
  if (!server().hasHeader(HEADER_IF_NONE_MATCH))
    return String();

  auto value = server().header(HEADER_IF_NONE_MATCH);
  DEBUG_ESP_PRINT(F("FOUND "));
  DEBUG_ESP_PRINT(HEADER_IF_NONE_MATCH);
  DEBUG_ESP_PRINT(F(": "));
  DEBUG_ESP_PRINTLN(value);

  if (!value.startsWith(ETAG_PREFIX) || !value.endsWith("\""))
    return String();

  return value.substring(strlen(ETAG_PREFIX), value.length() - 1);
}

bool ApiController::ifNoneMatchEquals(const String& value)
{
  if (!value.length())
    return false;

  String ifNoneMatch = parseIfNoneMatch();
  return ifNoneMatch.equals(value);
}

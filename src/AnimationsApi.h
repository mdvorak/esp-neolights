#pragma once

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <ApiController.h>
#include <Animations.h>
#include <AnimationStore.h>
#include <SystemManager.h>
#include <UrlFactory.h>
#include <DebugEsp.h>


// AnimationsApi class
class AnimationsApi : public ApiController
{
  private:
    static const char PATH_MENU[];
    static const char PATH_ANIMATIONS[];
    static const char PATH_SCHEDULE[];
    static const char PATH_ADMIN[];
    static const char PATH_RESTART[];
    static const char PATH_FACTORY_RESET[];

    static const char CACHE_CONTROL_DAY[];
    static const char CACHE_CONTROL_WEEK[];

  public:
    AnimationsApi(ESP8266WebServer& server, AnimationStore& store, SystemManager& systemManager);

    void begin()
    {
      // Note: This will conflict with other libraries
      const char* headerKeys[] = {HEADER_IF_NONE_MATCH, HEADER_X_FORWARDED_PROTO};
      server().collectHeaders(headerKeys, 2);

      // Register handlers
      server().on("/api", HTTP_GET, std::bind(&AnimationsApi::getRoot, this));
      server().on("/api/", HTTP_GET, std::bind(&AnimationsApi::getRoot, this));

      server().on(PATH_MENU, HTTP_GET, std::bind(&AnimationsApi::getMenu, this));
      server().on(PATH_ANIMATIONS, HTTP_GET, std::bind(&AnimationsApi::getAnimations, this));
      server().on(PATH_SCHEDULE, std::bind(&AnimationsApi::handleSchedule, this));
      server().on(PATH_ADMIN, std::bind(&AnimationsApi::onAdminConfig, this));
      server().on(PATH_RESTART, std::bind(&AnimationsApi::onAdminRestart, this));
      server().on(PATH_FACTORY_RESET, HTTP_GET, std::bind(&AnimationsApi::getAdminFactoryReset, this));
      server().on(PATH_FACTORY_RESET, HTTP_POST, std::bind(&AnimationsApi::postAdminFactoryReset, this));

      server().onNotFound(std::bind(&AnimationsApi::onNotFound, this));

      // Static files
      serveStatic();
    }

  private:
    AnimationStore& _store;
    SystemManager& _systemManager;

    void serveStatic();

    JsonObject& addMenuLink(UrlFactory& factory, JsonObject& links);

    void getRoot();
    void getMenu();

    void getAnimations();
    bool handleAnimationConfig();
    void getAnimationConfig(uint8_t slot, uint8_t animationId);
    void postAnimationConfig(uint8_t slot, uint8_t animationId);
    void getAnimationConfigType(uint8_t slot);

    void handleSchedule();
    void getSchedule();
    void postSchedule();

    void onAdminConfig();
    void onAdminRestart();

    void getAdminFactoryReset();
    void postAdminFactoryReset();

    void onNotFound();

    String animationUrl(UrlFactory& factory, uint8_t slot, uint8_t animationId);
};

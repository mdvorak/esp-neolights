#include "AnimationsApi.h"
#include <DebugEsp.h>
#include <vector>
#include <libb64/cencode.h>
#include <math.h>

// Constants
const char AnimationsApi::PATH_MENU[] = "/api/menu";
const char AnimationsApi::PATH_ANIMATIONS[] = "/api/animations";
const char AnimationsApi::PATH_SCHEDULE[] = "/api/schedule";
const char AnimationsApi::PATH_ADMIN[] = "/api/admin";
const char AnimationsApi::PATH_RESTART[] = "/api/admin/restart";
const char AnimationsApi::PATH_FACTORY_RESET[] = "/api/admin/factory-reset";

const char AnimationsApi::CACHE_CONTROL_DAY[] = "public, max-age=86400";
const char AnimationsApi::CACHE_CONTROL_WEEK[] = "public, max-age=604800";

static const char PATH_TYPES_P[] PROGMEM = "/types/";
static const char METHODS_GET_P[] PROGMEM = "GET";
static const char METHODS_GET_POST_P[] PROGMEM = "GET,POST";

static const char CACHE_CONTROL_HOUR_P[] PROGMEM = "public, max-age=3600";

static const char APPLICATION_PREFIX_P[] PROGMEM = "application/";
static const char JSON_FORMAT_SUFFIX_P[] PROGMEM = "+json";
static const char APPLICATION_MENU_JSON_P[] PROGMEM = "application/x.menu+json";
static const char APPLICATION_ANIMATIONS_JSON_P[] PROGMEM = "application/x.animations+json";
static const char APPLICATION_ANIMATION_TYPES_JSON_P[] PROGMEM = "application/x.animation.types+json";
static const char APPLICATION_SCHEDULE_JSON_P[] PROGMEM = "application/x.schedule+json";
static const char APPLICATION_ADMIN_JSON_P[] PROGMEM = "application/x.admin+json";
static const char APPLICATION_RESTART_JSON_P[] PROGMEM = "application/x.admin.restart+json";
static const char APPLICATION_FACTORY_RESET_JSON_P[] PROGMEM = "application/x.admin.factory-reset+json";

static const char NO_ANIMATION[] = "No Animation";

// Json
static const char LINK_NAV[] = "nav";
static const char LINK_PARENT[] = "parent";
static const char LINK_EDIT[] = "edit";
static const char LINK_COLLECTION[] = "collection";
static const char LINK_RESET[] = "reset";
static const char KEY_ACTIVE[] = "active";
static const char KEY_SELECTED[] = "selected";
static const char KEY_NAME[] = "name";
static const char KEY_LABEL[] = "label";
static const char KEY_BRIGHTNESS[] = "brightness";
static const char KEY_GAMMA[] = "gamma";
static const char KEY_COUNT[] = "count";
static const char KEY_FPS[] = "fps";
static const char KEY_FRAMES[] = "frames";
static const char KEY_COLOR[] = "color";
static const char KEY_PIXELS[] = "pixels";
static const char KEY_TIME_OFFSET[] = "timeOffset";
static const char KEY_NTP_UPDATE_INTERVAL[] = "ntpUpdateInterval";
static const char KEY_ENABLED[] = "enabled";
static const char KEY_SCHEDULE[] = "schedule";
static const char KEY_CURRENT[] = "current";
static const char KEY_BUILD[] = "build";
static const char KEY_SYSTEM_TIME[] = "systemTime";
static const char KEY_RSSI[] = "rssi";
static const char KEY_COLORS[] = "colors";
static const char KEY_REVERSE[] = "reverse";


// Methods
AnimationsApi::AnimationsApi(ESP8266WebServer& server, AnimationStore& store, SystemManager& systemManager)
  : ApiController(server),
    _store(store),
    _systemManager(systemManager)
{

}

void AnimationsApi::serveStatic()
{
  const char* indexPath = "/index.html";
  const char* const staticFiles[] = {
    indexPath,
    "/favicon.ico",
    "/inline.bundle.js",
    "/main.bundle.js",
    "/polyfills.bundle.js",
    "/vendor.bundle.js",
    "/styles.bundle.css",
  };
  size_t staticFilesLen = sizeof(staticFiles) / sizeof(*staticFiles);

  // Root
  server().serveStatic("/", SPIFFS, indexPath, CACHE_CONTROL_WEEK);

  // Static files
  for (size_t i = 0; i < staticFilesLen; i++) {
    DEBUG_ESP_PRINT(F("*SERVING STATIC "));
    DEBUG_ESP_PRINTLN(staticFiles[i]);
    server().serveStatic(staticFiles[i], SPIFFS, staticFiles[i], CACHE_CONTROL_WEEK);
  }
}

String AnimationsApi::animationUrl(UrlFactory& factory, uint8_t slot, uint8_t animationId)
{
  return factory.absoluteUrl((String(PATH_ANIMATIONS) + "/") + slot + String(PATH_TYPES_P) + animationId);
}

JsonObject& AnimationsApi::addMenuLink(UrlFactory& factory, JsonObject& links)
{
  links[LINK_NAV] = factory.absoluteUrl(PATH_MENU);
  return links;
}

void AnimationsApi::onNotFound()
{
#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.print(F("URI: "));
  DEBUG_ESP_PORT.println(server().uri());

  DEBUG_ESP_PORT.println(F("ARGS:"));
  for ( uint8_t i = 0; i < server().args(); i++ ) {
    DEBUG_ESP_PORT.println(String(" ") + server().argName(i) + ": " + server().arg(i));
  }
#endif

  static const String ALL_METHODS(F("GET,POST,PUT,DELETE"));
  if (handleOptions(ALL_METHODS)) {
    // Handled
    return;
  }

  // Special (unsupported by library) path identifiers
  if (handleAnimationConfig()) {
    // Handled
    return;
  }

  // Really not found
  sendStatus(404);
}

void AnimationsApi::getRoot()
{
  UrlFactory url(server());
  auto slot = _store.selectedSlot();

  DEBUG_ESP_PRINT(F("API ROOT TO SLOT "));
  DEBUG_ESP_PRINT(slot);

  if (slot == AnimationStore::DISABLED) {
    sendRedirect(303, url.absoluteUrl(PATH_MENU));
    return;
  }

  AnimationConfig animationConfig = {};
  _store.loadAnimationConfig(slot, animationConfig);

  DEBUG_ESP_PRINT(F(" TYPE "));
  DEBUG_ESP_PRINTLN(animationConfig.id);

#ifdef DEBUG_ESP_PORT
  sendAccessControlHeaders(METHODS_GET_P);
#endif

  sendRedirect(303, animationUrl(url, slot, animationConfig.id));
}

void AnimationsApi::getMenu()
{
  // Note: No authentication here

  // Prepare response
  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& items = root.createNestedArray(KEY_LINKS);

  UrlFactory url(server());

  // Add links
  {
    JsonObject& item = items.createNestedObject();
    item[KEY_LABEL] = "Animations";
    item[LINK_SELF] = url.absoluteUrl(PATH_ANIMATIONS);
  }

  {
    JsonObject& item = items.createNestedObject();
    item[KEY_LABEL] = "Schedule";
    item[LINK_SELF] = url.absoluteUrl(PATH_SCHEDULE);
  }

  {
    JsonObject& item = items.createNestedObject();
    item[KEY_LABEL] = "Admin";
    item[LINK_SELF] = url.absoluteUrl(PATH_ADMIN);
  }

  // Send
  server().sendHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_DAY);
  sendJson(root, APPLICATION_MENU_JSON_P);
}

void AnimationsApi::getAnimations()
{
  // Authenticate
  if (!authenticate()) {
    return server().requestAuthentication();
  }

  // Cache header
  if (ifNoneMatchEquals(String(_store.lastChange()))) {
    return sendStatus(304);
  }

  // Re-used variable for each animation config
  AnimationConfig animationConfig = {};
  AnimationInfo animationInfo = {};
  auto activeSlot = _store.selectedSlot();

  DEBUG_ESP_PRINT(F("SELECTED SLOT "));
  DEBUG_ESP_PRINTLN(activeSlot);

  // Prepare json object
  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& items = root.createNestedArray(KEY_ITEMS);

  UrlFactory url(server());

  // For each animation slot
  auto slots = _store.animationSlotsCount();
  for (uint8_t slot = 0; slot < slots; slot++) {
    DEBUG_ESP_PRINT(F("PROCESSING SLOT "));
    DEBUG_ESP_PRINT(slot);
    DEBUG_ESP_PRINT(F("\n "));

    // Get animation config
    _store.loadAnimationConfig(slot, animationConfig);

    DEBUG_ESP_PRINT(F(" FOUND ANIMATION "));
    DEBUG_ESP_PRINT(animationConfig.id);

    // Get animation info
    _store.animationInfo(animationConfig.id, animationInfo);

    DEBUG_ESP_PRINT(F(" WITH TYPE "));
    DEBUG_ESP_PRINT(animationInfo.type != NULL ? animationInfo.type : "NULL");

    // Construct JSON data object
    JsonObject& animation = items.createNestedObject();

    if (animationInfo.name != NULL) {
      animation[KEY_NAME] =  jsonBuffer.strdup(animationInfo.name);
      animation[KEY_LABEL] = jsonBuffer.strdup(animationConfig.label);
    } else {
      // We skip here user-defined label for the slot, since its considered disabled
      animation[KEY_NAME] = NO_ANIMATION;
    }

    if (slot == activeSlot) {
      animation[KEY_ACTIVE] = true;
    }

    // Links
    JsonObject& links = animation.createNestedObject(KEY_LINKS);
    String self = animationUrl(url, slot, animationConfig.id);
    links[LINK_SELF] = self;

    DEBUG_ESP_PRINT(F(" SELF "));
    DEBUG_ESP_PRINTLN(self);
  }

  // Send to server
  addMenuLink(url, root.createNestedObject(KEY_LINKS));

  sendETag(String(_store.lastChange()));
  sendJson(root, APPLICATION_ANIMATIONS_JSON_P);
}

bool AnimationsApi::handleAnimationConfig()
{
  String uri;

  {
    String pathAnimations(PATH_ANIMATIONS);
    pathAnimations.concat("/");

    // Validate URL start
    if (!server().uri().startsWith(pathAnimations)) {
      return false;
    }

    uri = server().uri().substring(pathAnimations.length());
  }

  DEBUG_ESP_PRINT(F("HANDLING ANIMATION CONFIG "));
  DEBUG_ESP_PRINTLN(uri);

  // Get slot
  char* relativeUri = NULL;
  uint8_t slot = static_cast<uint8_t>(strtoul(uri.c_str(), &relativeUri, 10));

  // If not parsed
  if (!relativeUri || relativeUri == uri.c_str()) {
    DEBUG_ESP_PRINTLN(F("FAILED TO PARSE SLOT"));
    return false;
  }

  DEBUG_ESP_PRINT(F("SLOT "));
  DEBUG_ESP_PRINTLN(slot);

  // Validate range
  if (slot > _store.animationSlotsCount()) {
    DEBUG_ESP_PRINTLN(F("SLOT OUT OF RANGE"));
    return false;
  }

  // Authenticate
  if (!authenticate()) {
    server().requestAuthentication();
    return true;
  }

  DEBUG_ESP_PRINT(F("TYPES URL "));
  DEBUG_ESP_PRINTLN(relativeUri ? relativeUri : "(null)");

  // Does it start with "types"?
  const char* TYPES = "/types";
  if (relativeUri == NULL || strncmp(relativeUri, TYPES, strlen(TYPES)) != 0) {
    // Redirect to specific animation type
    AnimationConfig animationConfig = {};
    _store.loadAnimationConfig(slot, animationConfig);

    DEBUG_ESP_PRINT(F("REDIRECTING TO SPECIFIC ANIMATION TYPE "));
    DEBUG_ESP_PRINTLN(animationConfig.id);

    UrlFactory url(server());
    sendRedirect(303, animationUrl(url, slot, animationConfig.id));
    return true;
  }

  // starts with "/types"
  relativeUri += strlen(TYPES);

  DEBUG_ESP_PRINT(F("HANDLING ANIMATION TYPE "));
  DEBUG_ESP_PRINTLN(relativeUri);

  if (*relativeUri == '\0' || strcmp(relativeUri, "/") == 0) {
    // Type selection
    if (server().method() == HTTP_GET) {
      getAnimationConfigType(slot);
    } else {
      sendStatus(405);
    }

    return true;
  } else if (*relativeUri == '/') {
    // Parse animationId
    relativeUri++;
    uint8_t animationId = static_cast<uint8_t>(strtoul(relativeUri, NULL, 10));

    // Animation config
    switch (server().method()) {
      case HTTP_GET:
        getAnimationConfig(slot, animationId);
        break;
      case HTTP_POST:
        // Free memory to avoid issues
        _store.releaseMemory();
        postAnimationConfig(slot, animationId);
        break;
      default:
        sendStatus(405);
        break;
    }

    return true;
  }

  // Not found
  DEBUG_ESP_PRINTLN(F("ANIMATION NOT FOUND"));
  return false;
}

// TODO simpify, don't include animationId in URLs, change types to form
void AnimationsApi::getAnimationConfig(uint8_t slot, uint8_t animationId)
{
  // Note: Already authenticated

  DEBUG_ESP_PRINT(F("GET SLOT CONFIG: "));
  DEBUG_ESP_PRINT(slot);
  DEBUG_ESP_PRINT(F(" ANIMATION ID: "));
  DEBUG_ESP_PRINTLN(animationId);

  // Load from eeprom
  AnimationConfig animationConfig = {};
  _store.loadAnimationConfig(slot, animationConfig);

  if (animationId >= _store.animationsCount()) {
    // Out of range, fallback to zero (always NoAnimation)
    DEBUG_ESP_PRINT(F("ANIMATION ID "));
    DEBUG_ESP_PRINT(animationId);
    DEBUG_ESP_PRINTLN(F(" OUT OF RANGE, SENDING DEFAULTS"));

    animationId = 0;
    animationConfig.clear();
  }

  // Get media type
  AnimationInfo animationInfo = {};
  _store.animationInfo(animationId, animationInfo);

  if (!animationInfo.type) {
    // Unsupported animation type
    DEBUG_ESP_PRINT(F("INVALID ANIMATION INFO FOR ID "));
    DEBUG_ESP_PRINTLN(animationId);

    sendStatus(500);
    return;
  }

  String mediaType = String(APPLICATION_PREFIX_P) + animationInfo.type + String(JSON_FORMAT_SUFFIX_P);

  // Print as json
  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  UrlFactory url(server());

  root[KEY_LABEL] = animationConfig.label;
  root[KEY_BRIGHTNESS] = constrain(animationConfig.brightness, 0, 100);
  if (!isnan(animationConfig.gamma)) root[KEY_GAMMA] = animationConfig.gamma;
  root[KEY_COUNT] = animationConfig.count;
  root[KEY_FPS] = animationConfig.fps;
  root[KEY_FRAMES] = animationConfig.frames;
  root[KEY_NAME] = animationInfo.name != NULL ? animationInfo.name : NO_ANIMATION; // read-only
  root[KEY_COLORS] = animationConfig.colors;
  root[KEY_REVERSE] = animationConfig.reverse;

  char colorStr[10] = {};
  ((HtmlColor) animationConfig.color).ToNumericalString(colorStr, sizeof(colorStr));
  root[KEY_COLOR] = colorStr;

  root[KEY_ACTIVE] = slot == _store.selectedSlot();

  // Links
  JsonObject& links = root.createNestedObject(KEY_LINKS);
  String self = animationUrl(url, slot, animationId);
  links[LINK_SELF] = self.c_str(); // This does not copy the string, but string must live longer then jsonBuffer

  String edit = url.absoluteUrl((String(PATH_ANIMATIONS) + "/") + slot + String("/types"));
  links[LINK_EDIT] = edit.c_str(); // This does not copy the string, but string must live longer then jsonBuffer

  // TODO add query param
  String collection = url.absoluteUrl(PATH_ANIMATIONS) + "?" + String(_store.lastChange());
  links[LINK_COLLECTION] = collection.c_str(); // This does not copy the string, but string must live longer then jsonBuffer

  addMenuLink(url, links);

  // Send to server
  sendJson(root, mediaType);
}


void AnimationsApi::postAnimationConfig(uint8_t slot, uint8_t animationId)
{
  // Note: Already authenticated

  DEBUG_ESP_PRINT(F("POST SLOT CONFIG: "));
  DEBUG_ESP_PRINT(slot);
  DEBUG_ESP_PRINT(F(" ANIMATION ID: "));
  DEBUG_ESP_PRINTLN(animationId);

  AnimationConfig animationConfig = {};

  if (animationId >= _store.animationsCount()) {
    // Invalid id
    DEBUG_ESP_PRINT(F("INVALID ANIMATION ID "));
    DEBUG_ESP_PRINTLN(animationId);

    sendStatus(400);
    return;
  }

  // Parse
  String payload = server().arg("plain");

  if (payload.length() < 2) {
    DEBUG_ESP_PRINTLN(F("EMPTY PAYLOAD"));
    sendStatus(400);
    return;
  }

  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(payload);
  if (!root.success()) {
    DEBUG_ESP_PRINTLN(F("PAYLOAD FAILED TO DECODE"));
    sendStatus(400);
    return;
  }

  // Read from JSON object
  animationConfig.id = animationId;
  animationConfig.brightness = constrain((int)root[KEY_BRIGHTNESS], 0, 100);
  animationConfig.gamma = root[KEY_GAMMA];
  animationConfig.count = root[KEY_COUNT];
  animationConfig.fps = root[KEY_FPS];
  animationConfig.frames = root[KEY_FRAMES];
  animationConfig.colors = root[KEY_COLORS];
  animationConfig.reverse = root[KEY_REVERSE];

  HtmlColor color;
  const String colorStr = root[KEY_COLOR];
  color.Parse<HtmlColorNames>(colorStr);
  animationConfig.color = color;

  const char *labelStr = root[KEY_LABEL];
  strncpy(animationConfig.label, labelStr, sizeof(animationConfig.label) - 1);
  animationConfig.label[sizeof(animationConfig.label) - 1] = '\0';

  // Store
  _store.storeAnimationConfig(slot, animationConfig);

  // Select
  if (root[KEY_ACTIVE]) {
    // Disable scheduler
    SystemConfig systemConfig = {};
    _systemManager.loadConfig(systemConfig);

    if (systemConfig.schedulerEnabled) {
      DEBUG_ESP_PRINTLN(F("DISABLING SCHEDULER"));

      systemConfig.schedulerEnabled = false;
      _systemManager.persistConfig(systemConfig);
    }

    // Persist selected
    _store.selectSlot(slot);
  }

  // OK
  UrlFactory url(server());
  sendRedirect(204, animationUrl(url, slot, animationId));
}

void AnimationsApi::getAnimationConfigType(uint8_t slot)
{
  // Note: Already authenticated

  DEBUG_ESP_PRINT(F("SLOT TYPE LIST: "));
  DEBUG_ESP_PRINTLN(slot);

  // Free memory to avoid issues
  _store.releaseMemory();

  // Prepare json object
  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& items = root.createNestedArray(KEY_ITEMS);

  UrlFactory url(server());

  // Add item for every type
  auto animationsCount = _store.animationsCount();
  AnimationInfo info = {};

  DEBUG_ESP_PRINT(F("FOUND "));
  DEBUG_ESP_PRINT(animationsCount);
  DEBUG_ESP_PRINTLN(F(" ANIMATION TYPES"));

  // Zero is NoAnimation
  for (uint8_t animationId = 0; animationId < animationsCount; animationId++) {
    _store.animationInfo(animationId, info);

    // Skip missing (NoAnimation has empty name)
    if (animationId > 0 && !info.name)
      continue;

    // Get nice name, including 0 NoAnimation
    const char* name = info.name != NULL ? info.name : NO_ANIMATION;

    DEBUG_ESP_PRINT(F(" ANIMATION NAME "));
    DEBUG_ESP_PRINTLN(name);

    // Add
    JsonObject& animation = items.createNestedObject();
    animation[KEY_NAME] = jsonBuffer.strdup(name);

    JsonObject& links = animation.createNestedObject(KEY_LINKS);
    String self = animationUrl(url, slot, animationId);
    links[LINK_SELF] = self;

    DEBUG_ESP_PRINT(F(" SELF "));
    DEBUG_ESP_PRINTLN(self);
  }

  // Add links
  auto& links = root.createNestedObject(KEY_LINKS);

  String detail = url.absoluteUrl((String(PATH_ANIMATIONS) + "/") + slot);
  links[LINK_PARENT] = detail.c_str(); // This does not copy the string, but string must live longer then jsonBuffer

  addMenuLink(url, links);

  // Send to server
  server().sendHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_HOUR_P);
  sendJson(root, APPLICATION_ANIMATION_TYPES_JSON_P);
}

void AnimationsApi::handleSchedule()
{
  switch (server().method()) {
    case HTTP_GET:
      getSchedule();
      break;

    case HTTP_POST:
      _store.releaseMemory();
      postSchedule();
      break;

    default:
      if (!handleOptions(METHODS_GET_POST_P)) {
        sendStatus(405);
      }
      break;
  }
}

void AnimationsApi::getSchedule()
{
  DEBUG_ESP_PRINTLN(F("GET SCHEDULE"));

  // Authenticate
  if (!authenticate()) {
    return server().requestAuthentication();
  }

  // Read current data - needed for update as well, we don't update entire config here
  SystemConfig config = {};
  _systemManager.loadConfig(config);

  // Create JSON
  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  UrlFactory url(server());

  root[KEY_ENABLED] = config.schedulerEnabled;
  root[KEY_CURRENT] = _systemManager.getCurrentSchedule(); // read-only
  root[KEY_SYSTEM_TIME] = _systemManager.getEpochTime(); // read-only

  JsonArray& schedule = root.createNestedArray(KEY_SCHEDULE);

  for (size_t i = 0; i < SystemConfig::SCHEDULE_LENGTH; i++) {
    schedule.add(config.schedule[i]);
  }

  // Send data
  auto& links = root.createNestedObject(KEY_LINKS);

  // Note: This is actually wrong approach, but its space-efficient
  String animations = url.absoluteUrl(PATH_ANIMATIONS);
  links[KEY_ITEMS] = animations.c_str();

  addMenuLink(url, links);
  sendJson(root, APPLICATION_SCHEDULE_JSON_P);
}


void AnimationsApi::postSchedule()
{
  DEBUG_ESP_PRINTLN(F("POST SCHEDULE"));

  // Authenticate
  if (!authenticate()) {
    return server().requestAuthentication();
  }

  // Read current data - needed for update as well, we don't update entire config here
  SystemConfig config = {};
  _systemManager.loadConfig(config);

  // Update data
  String payload = server().arg("plain");

  if (payload.length() < 2) {
    DEBUG_ESP_PRINTLN(F("EMPTY PAYLOAD"));
    sendStatus(400);
    return;
  }

  StaticJsonBuffer<2048> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(payload);

#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.println(F("PARSED PAYLOAD:"));
  root.printTo(DEBUG_ESP_PORT);
  DEBUG_ESP_PORT.println();
#endif

  if (root.containsKey(KEY_ENABLED)) {
    config.schedulerEnabled = root[KEY_ENABLED];
  }

  if (root.containsKey(KEY_SCHEDULE)) {
    DEBUG_ESP_PRINTLN(F("UPDATING SCHEDULE"));

    JsonArray& schedule = root[KEY_SCHEDULE];
    for (size_t i = 0; i < SystemConfig::SCHEDULE_LENGTH; i++) {
      config.schedule[i] = i < schedule.size() ? schedule[i] : AnimationStore::DISABLED;

#ifdef DEBUG_ESP_PORT
      auto segment = 1440 / SystemConfig::SCHEDULE_LENGTH * i;
      DEBUG_ESP_PRINT(segment / 600 ? "" : "0");
      DEBUG_ESP_PRINT(segment / 60);
      DEBUG_ESP_PRINT(":");
      DEBUG_ESP_PRINT((segment % 60) / 10 ? "" : "0");
      DEBUG_ESP_PRINT(segment % 60);
      DEBUG_ESP_PRINT(F(" SLOT "));
      DEBUG_ESP_PRINTLN(config.schedule[i]);
#endif
    }
  }

  // Store system config
  _systemManager.persistConfig(config);

  // OK
  sendStatus(204);
}

void AnimationsApi::onAdminConfig()
{
  DEBUG_ESP_PRINTLN(F("ADMIN CONFIG"));

  // Handle OPTIONS (must be before authentication)
  if (handleOptions(METHODS_GET_POST_P)) {
    return;
  }

  // Authenticate
  if (!authenticate()) {
    return server().requestAuthentication();
  }

  // Read current data - needed for update as well, we don't update entire config here
  SystemConfig config = {};
  _systemManager.loadConfig(config);

  StaticJsonBuffer<1024> jsonBuffer;

  UrlFactory url(server());

  auto method = server().method();
  if (method == HTTP_GET) {
    // Create JSON
    JsonObject& root = jsonBuffer.createObject();

    root[KEY_PIXELS] = config.pixels;
    root[KEY_TIME_OFFSET] = config.timeOffset;
    root[KEY_NTP_UPDATE_INTERVAL] = config.ntpUpdateInterval;

    // Special disabled/enabled
    root[KEY_ENABLED] = _store.selectedSlot() != AnimationStore::DISABLED;

    // Readonly stats
    root[KEY_RSSI] = WiFi.RSSI();
    root[KEY_SYSTEM_TIME] = _systemManager.getEpochTime();

    // Build number
#ifdef BUILD_TIMESTAMP
    root[KEY_BUILD] = BUILD_TIMESTAMP;
#endif

    // Links
    auto& links = addMenuLink(url, root.createNestedObject(KEY_LINKS));
    String restart = url.absoluteUrl(PATH_RESTART);
    links[LINK_EDIT] = restart.c_str(); // String needs to live as long as jsonBuffer

    String factoryReset = url.absoluteUrl(PATH_FACTORY_RESET);
    links[LINK_RESET] = factoryReset.c_str(); // String needs to live as long as jsonBuffer

    // Send data
    sendJson(root, APPLICATION_ADMIN_JSON_P);
  } else if (method == HTTP_POST) {
    // Update data
    String payload = server().arg("plain");

    if (payload.length() < 2) {
      DEBUG_ESP_PRINTLN(F("EMPTY PAYLOAD"));
      sendStatus(400);
      return;
    }

    // Copy for change decection
    SystemConfig oldConfig = config;
    bool restartNeeded = false;

    JsonObject& root = jsonBuffer.parseObject(payload);

#ifdef DEBUG_ESP_PORT
    DEBUG_ESP_PORT.println(F("PARSED PAYLOAD:"));
    root.printTo(DEBUG_ESP_PORT);
    DEBUG_ESP_PORT.println();
#endif

    config.pixels = root[KEY_PIXELS];
    config.timeOffset = root[KEY_TIME_OFFSET];
    config.ntpUpdateInterval = root[KEY_NTP_UPDATE_INTERVAL];
    bool enabled = false;

    // Special disabled/enabled
    if (root.containsKey(KEY_ENABLED)) {
      enabled = root[KEY_ENABLED];
      DEBUG_ESP_PRINT(F("GOT ENABLED: "));
      DEBUG_ESP_PRINTLN(enabled);
    } else {
      // Do nothing
      enabled = _store.selectedSlot() != AnimationStore::DISABLED;
      DEBUG_ESP_PRINTLN(F("NO ENABLED"));
    }

    // Detect change
    if (oldConfig != config) {
      // Store system config
      _systemManager.persistConfig(config);
      restartNeeded = true;
    }

    // Enable/disable
    if (enabled != (_store.selectedSlot() != AnimationStore::DISABLED)) {
      DEBUG_ESP_PRINTLN(F("ENABLED STATUS CHANGED"));

      if (enabled) {
        auto slot = _store.savedSlot();

        // If last persisted slot is disabled, enable first animation
        if (slot == AnimationStore::DISABLED) {
          slot = 0;
        }

        _store.selectSlot(slot, false); // Note: We don't persist here!
        _systemManager.restoreScheduler();
      } else {
        _store.selectSlot(AnimationStore::DISABLED, false); // Note: We don't persist here!
        _systemManager.suppressScheduler();
      }
    }

    // Note: For chages to take place, user needs to restart

    // Redirect to restart
    if (restartNeeded) {
      sendRedirect(204, url.absoluteUrl(PATH_RESTART));
    }else {
      sendStatus(204);
    }
  } else {
    sendStatus(405);
  }
}

void AnimationsApi::onAdminRestart()
{
  DEBUG_ESP_PRINTLN(F("ADMIN RESTART"));

  // Handle OPTIONS (must be before authentication)
  if (handleOptions(METHODS_GET_POST_P)) {
    return;
  }

  // Authenticate
  if (!authenticate()) {
    return server().requestAuthentication();
  }

  UrlFactory url(server());

  // Handle HTTP methods
  auto method = server().method();
  if (method == HTTP_POST) {
    DEBUG_ESP_PRINTLN(F("RESTARTING ESP"));

    // Send response
    sendRedirect(204, url.absoluteUrl(PATH_ADMIN));
    yield();

    // Restart - nothing else will happen after this!
    _systemManager.restart();
  } else if (method == HTTP_GET) {
    // Empty response with correct type
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();

    auto& links = addMenuLink(url, root.createNestedObject(KEY_LINKS));

    String admin = url.absoluteUrl(PATH_ADMIN);
    links[LINK_PARENT] = admin.c_str(); // String needs to live longer than jsonBuffer

    server().sendHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_WEEK);
    sendJson(root, APPLICATION_RESTART_JSON_P);
  } else {
    // Not supported
    sendStatus(405);
  }
}

void AnimationsApi::getAdminFactoryReset()
{
  // Empty response with correct type
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  UrlFactory url(server());
  auto& links = addMenuLink(url, root.createNestedObject(KEY_LINKS));

  String admin = url.absoluteUrl(PATH_ADMIN);
  links[LINK_PARENT] = admin.c_str(); // String needs to live longer than jsonBuffer

  server().sendHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_WEEK);
  sendJson(root, APPLICATION_FACTORY_RESET_JSON_P);
}

void AnimationsApi::postAdminFactoryReset()
{
  // Perform reset
  _systemManager.factoryReset();
  _store.factoryReset();

  // Note: For chages to take place, user needs to restart

  // Redirect to restart
  UrlFactory url(server());
  sendRedirect(204, url.absoluteUrl(PATH_RESTART));
}

/*
 NeoLights - Animated addressable LEDs WS2812B controller with web UI.
 Copyright (C) 2017  Michal Dvořák

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Arduino.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <FS.h>
#include <NeoPixelAnimator.h>
#include <NeoPixelBus.h>
#include <NTPClient.h>
#include <ArduinoOTA.h>

#include <Animations.h>
#include <Animations/RainbowAnimation.h>
#include <Animations/SolidColorAnimation.h>
#include <Animations/StarsAnimation.h>
#include <AnimationManager.h>
#include <SystemManager.h>
#include <DebugEsp.h>

#include "AnimationsApi.h"
#include "Config.h"

#define START_DELAY 5000

// Global services
static MyNeoPixelBus* strip;
static ESP8266WebServer server(80);
static SystemManager systemManager(SYSTEM_EEPROM_OFFSET, RELAY_PIN);
static AnimationManager<NeoGrbFeature> animationManager(SystemConfig::EEPROM_SIZE, ANIMATION_SLOTS);

static WiFiUDP ntpUDP;
static NTPClient timeClient(ntpUDP, NTP_SERVER);

static AnimationsApi animationsApi(server, animationManager, systemManager);


void setup() {
  // Status LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // Disable relay
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW);

  // Debug
#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.begin(115200);
  DEBUG_ESP_PORT.println();
#endif

  // Register animations - note that order is significant
  animationManager.registerAnimation<RainbowAnimation>();
  animationManager.registerAnimation<SolidColorAnimation>();
  animationManager.registerAnimation<StarsAnimation>();
  animationManager.registerAnimation<ShootingStarsAnimation>();

  DEBUG_ESP_PRINT(F("ANIMATION COUNT: "));
  DEBUG_ESP_PRINTLN(animationManager.animationsCount());
  DEBUG_ESP_PRINTLN();

  // Reset WiFi
  WiFi.disconnect(true);

  // Start connecting
  {
    IPAddress ip(IP_ADDRESS);
    IPAddress mask(IP_MASK);
    IPAddress gw(IP_GATEWAY);
    IPAddress dns(IP_DNS);

    WiFi.mode(WIFI_STA);
    WiFi.config(ip, gw, mask, dns);

    DEBUG_ESP_PRINT(F("CONNECTING TO "));
    DEBUG_ESP_PRINTLN(WIFI_SSID);

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  }

  // Wait for connection
  auto start = millis();
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(200);
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
  }

  DEBUG_ESP_PRINTLN(F("CONNECTED TO WIFI"));
  DEBUG_ESP_PRINTLN(WiFi.localIP().toString());

  // Safety relay delay
  while (millis() - start < START_DELAY) {
    delay(50);
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
  }

  // Start EEPROM
  EEPROM.begin(SYSTEM_EEPROM_OFFSET + SystemConfig::EEPROM_SIZE + animationManager.eepromSize());

  // Load config
  SystemConfig neoLightsConfig = {};
  systemManager.loadConfig(neoLightsConfig);

  // Reset initial animation if crash is detected
  struct rst_info* rst_info = ESP.getResetInfoPtr();
  if (rst_info->reason == REASON_SOFT_WDT_RST ||
      rst_info->reason == REASON_EXCEPTION_RST ||
      rst_info->reason == REASON_WDT_RST)
  {
    DEBUG_ESP_PRINTLN(F("CRASH DETECTED, RESETTING ANIMATION, SUPPRESSING SCHEDULER"));
    animationManager.resetActiveAnimation();
    systemManager.suppressScheduler();
  }

  // OTA
  ArduinoOTA.setHostname(OTA_HOSTNAME);
  ArduinoOTA.setPassword(OTA_PASSOWRD);

  // Diagnostic handlers
#ifdef DEBUG_ESP_PORT
    ArduinoOTA.onStart([]() {
        DEBUG_ESP_PRINT(timeClient.getFormattedTime());
        DEBUG_ESP_PRINTLN(" Starting OTA update at ");

        SPIFFS.end();
    });
    ArduinoOTA.onEnd([]() {
        DEBUG_ESP_PRINT(timeClient.getFormattedTime());
        DEBUG_ESP_PRINTLN(" OTA update completed"); 

        SPIFFS.begin();
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        DEBUG_ESP_PRINT(timeClient.getFormattedTime());
        DEBUG_ESP_PRINTF(" OTA update progress: %u%%\n", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        SPIFFS.begin();

        DEBUG_ESP_PRINT(timeClient.getFormattedTime());
        DEBUG_ESP_PRINTF(" OTA update failed: [%u] ", error);

        if (error == OTA_AUTH_ERROR)
            DEBUG_ESP_PRINTLN("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)
            DEBUG_ESP_PRINTLN("Begin Failed");
        else if (error == OTA_CONNECT_ERROR)
            DEBUG_ESP_PRINTLN("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR)
            DEBUG_ESP_PRINTLN("Receive Failed");
        else if (error == OTA_END_ERROR)
            DEBUG_ESP_PRINTLN("End Failed");
    });
#endif

  // Create strip
  // Note: We never release this, we need restart to change of config
  DEBUG_ESP_PRINT(F("INITIALIZING FOR PIXELS: "));
  DEBUG_ESP_PRINTLN(neoLightsConfig.pixels);

  strip = new MyNeoPixelBus(neoLightsConfig.pixels);

  // Begin components
  SPIFFS.begin();
  animationsApi.requireAuthentication(WWW_USERNAME, WWW_PASSWORD);
  animationsApi.begin();
  systemManager.begin(timeClient);
  timeClient.begin();
  server.begin();
  strip->Begin();
  ArduinoOTA.begin();

  // Initialized
  systemManager.enable();
  digitalWrite(LED_PIN, HIGH);

  DEBUG_ESP_PRINTLN(F("INITIALIZED"));

  // Wait for power (and set to white, 1/4 brightness)
  strip->ClearTo(RgbColor(64, 64, 64));
  start = millis();

  strip->Show();
  delay(10);

  // In the meantime, initialize NTP
  timeClient.forceUpdate();

  while (millis() - start < 3000) {
    strip->Show();
    delay(10);
  }

  DEBUG_ESP_PRINT(F("STARTED AT "));
  DEBUG_ESP_PRINTLN(timeClient.getFormattedTime());
  DEBUG_ESP_PRINTLN();
}

void loop() {
  uint8_t slot;

  // Handle scheduler
  if (systemManager.schedulerEnabled()) {
    slot = systemManager.getScheduledSlot();
    animationManager.selectSlot(slot);
  } else {
    slot = animationManager.selectedSlot();
  }

  // Handle power-on/off
  static bool lastEnabled = true;
  bool enabled;

  if (slot != AnimationStore::DISABLED) {
    systemManager.enable();
    enabled = true;
  } else {
    systemManager.disable();
    enabled = false;
  }

  // Show frame
  if (enabled) {
    strip->Show();
    lastEnabled = true;
  } else if (lastEnabled) {
    // Turn off leds, in case they are still running
    strip->ClearTo(RgbColor(0, 0, 0));
    strip->Show();
    lastEnabled = false;
  }

  // NTP
  timeClient.update();

  // WebServer
  server.handleClient();

  // OTA
  ArduinoOTA.handle();

  if (enabled) {
    // Flash led
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));

    // Animate
    animationManager.animate(*strip);
  } else {
    // Disabled
    digitalWrite(LED_PIN, LOW);
    delay(5);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedView } from 'angular-resource-router';
import { AnimationDetail } from './animations.data';
import { ApiClient } from '../api-client';

let uniqueId = 0;

@Component({
  templateUrl: './solidcolor.component.html'
})
export class SolidColorComponent implements OnInit {
  readonly id = `solidcolor-${uniqueId++}`;
  detail: AnimationDetail;

  constructor(private view: ActivatedView<AnimationDetail>,
              private apiClient: ApiClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.detail = data.body;
    });
  }

  onSubmit(): void {
    this.apiClient.saveViewData(this.view, this.detail);
  }
}

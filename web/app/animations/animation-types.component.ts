import { Component, OnInit } from '@angular/core';
import { AnimationTypes } from './animations.data';
import { ActivatedView } from 'angular-resource-router';

let uniqueId = 0;

@Component({
  template: `<h2>Select Animation Type</h2>

  <a [resourceLink]="types?._links?.parent" class="pure-button">
    <i class="fa fa-chevron-left" aria-hidden="true"></i> Back
  </a>
  <a *ngFor="let item of types?.items"
     [resourceLink]="item._links?.self" class="pure-button">
    {{item.name}}
  </a>
  `,
  styles: [`a {
    margin: .2em;
  }`]
})
export class AnimationTypesComponent implements OnInit {
  readonly id = `animation-types-${uniqueId++}`;
  types: AnimationTypes;

  constructor(private view: ActivatedView<AnimationTypes>) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.types = data.body;
    });
  }
}

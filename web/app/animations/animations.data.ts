export interface AnimationInfo {
  name: string;
  label: string;
  selected: boolean;
  active: boolean;

  _links?: {
    self: string;
  };
}

export interface Animations {
  items: AnimationInfo[];
}

export interface AnimationDetail {
  label: string;
  name: string;
  active: boolean;
  brightness?: number;
  gamma?: number;
  count?: number;
  fps?: number;
  frames?: number;
  color?: string;
  colors: boolean;
  reverse: boolean;

  _links?: {
    self: string;
    edit?: string;
  };
}

export interface AnimationType {
  name: string;

  _links?: {
    self: string;
  };
}

// TODO add slot here to the API
export interface AnimationTypes {
  items: AnimationType[];

  _links?: {
    self: string;
    parent?: string;
  };
}

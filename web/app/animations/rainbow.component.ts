import { Component, OnInit } from '@angular/core';
import { AnimationDetail } from './animations.data';
import { ApiClient } from '../api-client';
import { ActivatedView } from 'angular-resource-router';

let uniqueId = 0;

@Component({
  templateUrl: './rainbow.component.html'
})
export class RainbowComponent implements OnInit {
  readonly id = `rainbow-${uniqueId++}`;
  detail: AnimationDetail;

  constructor(private view: ActivatedView<AnimationDetail>,
              private apiClient: ApiClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.detail = data.body;
    });
  }

  onSubmit(): void {
    this.apiClient.saveViewData(this.view, this.detail);
  }
}

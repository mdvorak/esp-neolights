import { Component, Input } from '@angular/core';
import { AnimationDetail } from './animations.data';

@Component({
  selector: 'app-animation-controls',
  template: `
    <div class="pure-controls">
      <button type="submit" class="pure-button pure-button-primary"
              (click)="data.active=false;true">
        <i class="fa fa-floppy-o" aria-hidden="true"></i> Save
      </button>
      <button type="submit" class="pure-button pure-button-primary"
              (click)="data.active=true">
        <i class="fa fa-play" aria-hidden="true"></i> Activate
      </button>

      <a *ngIf="data._links?.edit" [resourceLink]="data._links?.edit"
         class="pure-button"><i class="fa fa-cog" aria-hidden="true"></i> Select Type</a>
    </div>`
})
export class AnimationControlsComponent {

  @Input() data: AnimationDetail;
}

import { Component, OnInit } from '@angular/core';
import { Animations } from './animations.data';
import { ActivatedView } from 'angular-resource-router';

let uniqueId = 0;

@Component({
  templateUrl: './animations.component.html',
  styles: [
    `.anim-name {
      display: block;
      font-style: italic;
      font-size: 75%;
    }`,
    `.anim-active {
      background: lightgray;
    }`
  ]
})
export class AnimationsComponent implements OnInit {
  readonly id = `animations-${uniqueId++}`;
  animations: Animations;

  constructor(private view: ActivatedView<Animations>) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.animations = data.body;
    });
  }
}

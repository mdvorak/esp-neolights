import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AnimationsComponent } from './animations.component';
import { AnimationTypesComponent } from './animation-types.component';
import { NoAnimationComponent } from './noanimation.component';
import { SolidColorComponent } from './solidcolor.component';
import { RainbowComponent } from './rainbow.component';
import { StarsComponent } from './stars.component';
import { AppComponentsModule } from '../components/components.module';
import { ResourceRouterModule } from 'angular-resource-router';
import { AnimationControlsComponent } from './animation-controls.component';

@NgModule({
  declarations: [
    AnimationsComponent,
    AnimationTypesComponent,
    AnimationControlsComponent,
    NoAnimationComponent,
    SolidColorComponent,
    RainbowComponent,
    StarsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppComponentsModule,
    ResourceRouterModule.forTypes([
      {
        type: 'application/x.animations',
        component: AnimationsComponent
      },
      {
        type: 'application/x.animation.types',
        component: AnimationTypesComponent
      },
      {
        type: 'application/x.animation.none',
        component: NoAnimationComponent
      },
      {
        type: 'application/x.animation.solidcolor',
        component: SolidColorComponent
      },
      {
        type: 'application/x.animation.rainbow',
        component: RainbowComponent
      },
      {
        type: 'application/x.animation.stars',
        component: StarsComponent
      }
    ])
  ]
})
export class AnimationsModule {
}

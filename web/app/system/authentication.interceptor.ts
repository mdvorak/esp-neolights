import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './authentication.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthorizedHttpInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Append basic auth
    const token = this.authenticationService.token;
    if (token) {
      req = req.clone({
        setHeaders: {
          authorization: `Basic ${token}`
        }
      });
    }

    return next.handle(req);

  }
}

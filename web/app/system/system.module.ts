import { NgModule } from '@angular/core';
import { ResourceRouterModule } from 'angular-resource-router';
import { AuthenticationService } from './authentication.service';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthorizedHttpInterceptor } from './authentication.interceptor';

@NgModule({
  declarations: [
    LoginComponent
  ],
  providers: [
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizedHttpInterceptor,
      multi: true
    }
  ],
  imports: [
    FormsModule,
    ResourceRouterModule.forTypes([
      {
        status: 401,
        type: '*',
        component: LoginComponent
      }
    ])
  ]
})
export class SystemModule {
}

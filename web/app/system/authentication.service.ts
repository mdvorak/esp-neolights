import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationService {

  token: string;

  login(credentials: Credentials) {
    this.token = btoa(credentials.username + ':' + credentials.password);
  }
}

export interface Credentials {
  username: string;
  password: string;
}

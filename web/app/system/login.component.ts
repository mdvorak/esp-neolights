import { Component } from '@angular/core';
import { AuthenticationService, Credentials } from './authentication.service';
import { ActivatedView } from 'angular-resource-router';

let uniqueId = 0;

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {

  readonly id = `login-${uniqueId++}`;

  data: Credentials = {
    username: '',
    password: ''
  };

  constructor(private authenticationService: AuthenticationService,
              private view: ActivatedView<any>) {
  }

  onSubmit() {
    this.authenticationService.login(this.data);
    this.view.reload();
  }
}

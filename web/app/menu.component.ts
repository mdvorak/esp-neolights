import { Component, OnInit } from '@angular/core';
import { ActivatedView } from 'angular-resource-router';

@Component({
  template: `
    <nav class="pure-menu pure-menu-horizontal">
      <ul class="pure-menu-list">
        <li *ngFor="let item of links"
            class="pure-menu-item">
          <a class="pure-menu-link"
             [resourceLink]="item.self">{{item.label}}</a>
        </li>
      </ul>
    </nav>`
})
export class MenuComponent implements OnInit {
  links: Array<{ label: string; self: string; }>;

  constructor(private readonly view: ActivatedView<any>) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => this.links = data.body ? data.body._links || [] : []);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedView } from 'angular-resource-router';

export interface WithSelfLink {
  _links?: {
    self?: string;
  };
}

@Injectable()
export class ApiClient {

  constructor(private http: HttpClient) {
  }

  saveViewData<T extends WithSelfLink>(view: ActivatedView<T>, data: T) {
    if (!data) {
      throw new Error('data must be defined');
    }

    // Determine URL
    const url = (data._links && data._links.self) || view.snapshot.url;
    if (!url) {
      throw new Error('Unable to determine target url');
    }

    // Post and navigate on response
    return this.http.post(url, data, {observe: 'response'})
      .subscribe(response => {
        const location = response.headers.get('location');
        view.navigation.go(location || url);
      });
  }
}

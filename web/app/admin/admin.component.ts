import { Component, OnInit } from '@angular/core';
import { ActivatedView } from 'angular-resource-router';
import { ApiClient } from '../api-client';

let uniqueId = 0;

export interface AdminData {
  pixels: number;
  timeOffset: number;
  ntpUpdateInterval: number;
  enabled: boolean;
  rssi: number;
  systemTime: number;
  build?: number;

  _links?: {
    self: string,
    edit?: string,
    reset?: string
  };
}

@Component({
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnInit {
  readonly id = `admin-${uniqueId++}`;
  config: AdminData;

  constructor(private view: ActivatedView<AdminData>,
              private apiClient: ApiClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.config = data.body;
    });
  }

  onSubmit(): void {
    this.apiClient.saveViewData(this.view, this.config);
  }
}

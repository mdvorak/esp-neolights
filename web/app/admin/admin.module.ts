import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ResourceRouterModule } from 'angular-resource-router';
import { AdminComponent } from './admin.component';
import { AdminRestartComponent } from './admin-restart.component';
import { AppComponentsModule } from '../components/components.module';
import { AdminFactoryResetComponent } from './admin-factory-reset.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminRestartComponent,
    AdminFactoryResetComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppComponentsModule,
    ResourceRouterModule.forTypes([
      {
        type: 'application/x.admin',
        component: AdminComponent
      },
      {
        type: 'application/x.admin.restart',
        component: AdminRestartComponent
      },
      {
        type: 'application/x.admin.factory-reset',
        component: AdminFactoryResetComponent
      }
    ])
  ]
})
export class AdminModule {
}

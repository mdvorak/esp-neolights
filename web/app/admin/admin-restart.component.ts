import { Component, OnInit } from '@angular/core';
import { ActivatedView } from 'angular-resource-router';
import 'rxjs/add/operator/delay';
import { HttpClient } from '@angular/common/http';

let uniqueId = 0;

export interface AdminRestart {
  _links?: {
    self: string;
    parent?: string;
  };
}


@Component({
  template: `<h2>System Restart</h2>
  <form (ngSubmit)="onSubmit()" class="pure-form">
    <div class="pure-controls">
      <em>Restart might take up to 30 seconds.</em>
    </div>
    <div class="pure-controls">
      <a [resourceLink]="config._links?.parent" class="pure-button"><i class="fa fa-chevron-left"
                                                                       aria-hidden="true"></i>
        Back</a>
      <button type="submit" class="pure-button button-danger"
              [disabled]="restarting"><i class="fa fa-undo" aria-hidden="true"></i> Restart
      </button>
    </div>
  </form>`,
  styles: [`.button-danger {
    background: rgb(202, 60, 60);
  }`]
})
export class AdminRestartComponent implements OnInit {
  readonly id = `admin-restart-${uniqueId++}`;
  config: AdminRestart;
  restarting: boolean;

  constructor(private view: ActivatedView<AdminRestart>,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.config = data.body;
    });
  }

  onSubmit(): void {
    this.restarting = true;

    // Determine URL
    const url = (this.config._links && this.config._links.self) || this.view.snapshot.url;
    if (!url) {
      throw new Error('Unable to determine target url');
    }

    // Post and reload after 30 secs
    this.http.post(url, this.config, {observe: 'response'})
      .delay(30000)
      .subscribe(() => {
        // Refresh browser
        window.location.reload();
      });
  }
}

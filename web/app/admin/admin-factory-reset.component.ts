import { Component, OnInit } from '@angular/core';
import { ActivatedView } from 'angular-resource-router';
import { ApiClient } from '../api-client';

let uniqueId = 0;

export interface AdminFactoryReset {
  _links?: {
    self: string;
    parent?: string;
  };
}


@Component({
  templateUrl: './admin-factory-reset.component.html',
  styles: [`.button-danger {
    background: rgb(202, 60, 60);
  }`]
})
export class AdminFactoryResetComponent implements OnInit {
  readonly id = `admin-factory-reset-${uniqueId++}`;
  config: AdminFactoryReset;

  constructor(private view: ActivatedView<AdminFactoryReset>,
              private apiClient: ApiClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.config = data.body;
    });
  }

  onSubmit(): void {
    this.apiClient.saveViewData(this.view, this.config);
  }
}

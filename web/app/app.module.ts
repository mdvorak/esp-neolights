import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ResourceRouterModule } from 'angular-resource-router';
import { AppComponent } from './app.component';
import { AnimationsModule } from './animations/animations.module';
import { ScheduleModule } from './schedule/schedule.module';
import { AdminModule } from './admin/admin.module';
import { SystemModule } from './system/system.module';
import { MenuComponent } from './menu.component';
import { ApiClient } from './api-client';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ResourceRouterModule.configure({
      prefix: environment.apiPrefix,
      useHash: true
    }),
    ResourceRouterModule.forTypes([
      {
        type: 'application/x.menu',
        component: MenuComponent
      }
    ]),

    SystemModule,
    AnimationsModule,
    ScheduleModule,
    AdminModule
  ],
  providers: [
    ApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

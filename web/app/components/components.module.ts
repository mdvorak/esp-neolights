import { NgModule } from '@angular/core';
import { SwitchButtonComponent } from './switch-button.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SwitchButtonComponent
  ],
  imports: [
    FormsModule
  ],
  exports: [
    SwitchButtonComponent
  ]
})
export class AppComponentsModule {
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'scheduleTimestamp', pure: true})
export class ScheduleTimestampPipe implements PipeTransform {
  transform(value: number, count: number): any {
    const segment = (1440 / count * value) % 1440;
    const hours = Math.floor(segment / 60);
    const minutes = Math.floor(segment % 60);

    return (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
  }
}

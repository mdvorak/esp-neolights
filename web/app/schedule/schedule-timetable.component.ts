import { Component, Input } from '@angular/core';
import { AnimationInfo } from '../animations/animations.data';

let uniqueId = 0;

@Component({
  selector: 'app-schedule-timetable',
  templateUrl: './schedule-timetable.component.html',
  styles: [
    `div.active select {
      background: rgb(28, 184, 65);
    }`,
    `.button-small {
      font-size: 50%;
    }`
  ]
})
export class ScheduleTimetableComponent {

  readonly id = `schedule-timetable-${uniqueId++}`;
  @Input() data: number[] = [];
  @Input() start = 0;
  @Input() end: number;
  @Input() current: number;
  @Input() selection: AnimationInfo[] = [];

  setValue(index: number, value: number): void {
    this.data[index] = value;
  }

  //noinspection JSMethodCanBeStatic
  identity(value: any): any {
    return value;
  }
}

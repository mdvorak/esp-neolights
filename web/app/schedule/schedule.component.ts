import { Component, OnInit } from '@angular/core';
import { Animations } from '../animations/animations.data';
import { ActivatedView } from 'angular-resource-router';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

let uniqueId = 0;


export interface Schedule {
  enabled: boolean;
  current: number;
  schedule: number[];
  systemTime: number;

  _links: {
    self: string;
    items: string;
  };
}


@Component({
  templateUrl: './schedule.component.html'
})
export class ScheduleComponent implements OnInit {
  readonly id = `schedule-${uniqueId++}`;
  data: Schedule;
  animations?: Animations;

  constructor(private view: ActivatedView<Schedule>,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    this.view.data.subscribe(data => {
      this.data = data.body;

      const itemsUrl = this.data._links['items'];
      if (itemsUrl) {
        this.http.get<Animations>(itemsUrl).subscribe(response => this.animations = response);
      }
    });
  }

  onSubmit(): void {
    const url = this.data._links['self'] || this.view.snapshot.url;
    this.http.post(url, this.data, {observe: 'response'})
      .subscribe(response => {
        const location = response.headers.get('Location');
        this.view.navigation.go(location || url);
      });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ResourceRouterModule } from 'angular-resource-router';
import { ScheduleTimestampPipe } from './schedule-timestamp.pipe';
import { ScheduleComponent } from './schedule.component';
import { ScheduleTimetableComponent } from './schedule-timetable.component';
import { AppComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    ScheduleComponent,
    ScheduleTimetableComponent,
    ScheduleTimestampPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppComponentsModule,
    ResourceRouterModule.forTypes([
      {
        type: 'application/x.schedule',
        component: ScheduleComponent
      }
    ])
  ]
})
export class ScheduleModule {
}
